<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//Validacion para el modelo de Usuario (Login , Cambio de Clave , CRUD Usuario)
class Menu_lib {

    function __construct(){
    	$this->CI =& get_instance();//Esto es para acceder ala instancia que carga la Lib (pasa los valores por referencia)
    }

    public function my_validation($registro) {
        $ctr = ($registro['controlador'] != '');
        $acc = ($registro['accion'] != '');
        $url = ($registro['url'] != '');

        // No puede no ingresar Controlador, Accion, URL
        if(!$ctr AND !$acc AND !$url) {
            $this->CI->form_validation->set_message('my_validation', 'Debe ingresar Controlador y Acción o una URL');
            return FALSE;
        }

        // Si ingresó controlador, debe ingresar accion
        if($ctr AND !$acc) {
            $this->CI->form_validation->set_message('my_validation', 'Ingresó Controlador, falta la Acción');
            return FALSE;
        }

        // Si ingresó accion, debe ingresar controlador
        if(!$ctr AND $acc) {
            $this->CI->form_validation->set_message('my_validation', 'Ingresó Acción, falta en Controlador');
            return FALSE;
        }

        // Si ingresó URL, no puede haber ni Controlador ni Accion
        if($url AND ($ctr OR $acc)) {
            $this->CI->form_validation->set_message('my_validation', 'Si ingresa URL, no ingresar ni Controlador ni Acción');
            return FALSE;
        }

        return TRUE;
    }
}