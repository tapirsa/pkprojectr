<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$lang['home_nosotros_heading']      = 'Quienes somos...';
$lang['home_nosotros_subheading']   = 'NUESTRA MISION ES BRINDAR SOLUCIONES DE ALTA COMPLEJIDAD, COMPROMETIDAS CON LAS NECESIDADES DEL MERCADO Y DE NUESTROS CLIENTES';


$lang['home_nosotros_desarrollo']   = "Soluciones Informáticas Integrales fue fundada en el año 1993. Hoy es una organización con más de 18 años de 
                                        experiencia en el mercado, y más de 70 personas dedicadas a la prestación de servicios de informática y la comercialización 
                                        de equipamiento y aplicaciones; para responder a los variados requerimientos de sus clientes. Cuenta con una sólida 
                                        estructura gerencial, técnica, administrativa y de recursos humanos. Su know-how y experiencia son íntegramente aplicados 
                                        en la prestación de servicios a sus clientes.";


$lang['home_h1_NuetrosServisios'] = 'Nuestros servicios';
$lang['home_p_NuetrosServisios'] = '	Consultoría en nuevas tecnologias. <br>
										Diseño, desarrollo e implementación de soluciones de IT(Information Technology).';

$lang['home_h1_Beneficios'] = 'Beneficios';
$lang['home_p_Beneficios'] = '	Logre progresos en materia de eficiencia, crecimiento y beneficios,
								posibles únicamente a través de la informatización de sus actividades.';

$lang['home_h1_NuevoContexto'] = 'Nuevo Contexto';
$lang['home_p_NuevoContexto'] = '	El mundo evolucionó a un lugar en donde todas las actividades del quehacer 
									humano fueron alcanzadas por las tecnologías informáticas. ';

$lang['home_h1_Internet'] = 'Internet y su impacto en nuestras vidas';
$lang['home_p_Internet'] = '	Conozca al instante las existencias para cada producto en cada sucursal 
										en cualquier ubicación geográfica donde éstas se encuentren gracias a las 
										nuevas comunicaciones existentes hoy en dia';

$lang['home_h1_NuevosDisp'] = 'Nuevos Dispositivos';
$lang['home_p_NuevosDisp'] = '	Obtenga acceso inmediato a todos los datos importantes para la dirección 
								de su negocio e interactue con sus clientes a traves de la mayoria de la 
								gran variedad de dispositivos existentes en la actualidad ';

$lang['home_h1_NuestrasEmpresa'] = 'Nuestra Empresa';
$lang['home_p_NuestrasEmpresa'] = '	Obtenga acceso inmediato a todos los datos importantes para la dirección 
									de su negocio e interactue con sus clientes a traves de la mayoria de la 
									gran variedad de dispositivos existentes en la actualidad';
