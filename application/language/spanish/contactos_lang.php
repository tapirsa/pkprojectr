<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// spanish

$lang['contacto_heading']           = 'Contáctenos';
$lang['contacto_subheading']        = 'Si lo desea, complete sus datos en el siguiente formulario y a la brevedad nos pondremos en contacto.';

$lang['contacto_msj_required_label']= '* Indica datos obligatorios';   

$lang['contacto_nombre_label']      = 'Nombre:* ';
$lang['contacto_apellido_label']    = 'Apellido: ';
$lang['contacto_email_label']       = 'Email:* ';
$lang['contacto_phone_label']       = 'Teléfono: ';
$lang['contacto_subject_label']     = 'Asunto:* ';
$lang['contacto_mensaje_label']     = 'Mensaje:* ';

$lang['contacto_nombre_placeholder']      = 'nombres';
$lang['contacto_apellido_placeholder']    = 'apellidos';
$lang['contacto_email_placeholder']       = 'dieccion de e-mail';
$lang['contacto_phone_placeholder']       = '# de telefono';
$lang['contacto_subject_placeholder']     = 'asunto del mesaje';
$lang['contacto_mensaje_placeholder']     = 'mensaje';

$lang['add_contacto_validation_nombre_label']       = 'Nombre';
$lang['add_contacto_validation_apellido_label']     = 'Apellido';
$lang['add_contacto_validation_email_label']        = 'Correo electronico';
$lang['add_contacto_validation_phone_label']        = 'Tercera parte del numero telefonico';
$lang['add_contacto_validation_subject_label']      = 'Asunto';
$lang['add_contacto_validation_mensaje_label']      = 'Mensaje';

$lang['enviar_contacto_submit_btn']                 = 'Enviar';