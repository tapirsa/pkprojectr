<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// englesh

$lang['contacto_heading']           = 'Contáctenos';
$lang['contacto_subheading']        = 'Enviar Mensaje a xxxx S.R.L.';

$lang['contacto_nombre_label']      = 'First Name: ';
$lang['contacto_apellido_label']    = 'Last Name: ';
$lang['contacto_email_label']       = 'Email: ';
$lang['contacto_phone_label']       = 'Phone: ';
$lang['contacto_subject_label']     = 'Subject: ';
$lang['contacto_mensaje_label']     = 'Message: ';


$lang['add_contacto_validation_nombre_label']       = 'Nombre';
$lang['add_contacto_validation_apellido_label']     = 'Apellido';
$lang['add_contacto_validation_email_label']        = 'Correo electronico';
$lang['add_contacto_validation_phone_label']        = 'Tercera parte del numero telefonico';
$lang['add_contacto_validation_subject_label']      = 'Asunto';
$lang['add_contacto_validation_mensaje_label']      = 'Mensaje';

$lang['enviar_contacto_submit_btn']                 = 'Enviar';