<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$lang['home_nosotros_heading']      = 'Quienes somos...';
$lang['home_nosotros_subheading']   = 'NUESTRA MISION ES BRINDAR SOLUCIONES DE ALTA COMPLEJIDAD, COMPROMETIDAS CON LAS NECESIDADES DEL MERCADO Y DE NUESTROS CLIENTES';


$lang['home_nosotros_desarrollo']   = "Soluciones Informáticas Integrales fue fundada en el año 1993. Hoy es una organización con más de 18 años de 
                                        experiencia en el mercado, y más de 70 personas dedicadas a la prestación de servicios de informática y la comercialización 
                                        de equipamiento y aplicaciones; para responder a los variados requerimientos de sus clientes. Cuenta con una sólida 
                                        estructura gerencial, técnica, administrativa y de recursos humanos. Su know-how y experiencia son íntegramente aplicados 
                                        en la prestación de servicios a sus clientes.";