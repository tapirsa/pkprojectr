<?php 
$config = array(
        'grupos/agregar' => array(
                           array(
                                   'field' => 'name',
                                   'label' => 'Nombre',
                                   'rules' => 'required|max_length[20]|xss_clean'
                                ),
                           array(
                                   'field' => 'description',
                                   'label' => 'Descripcion',
                                   'rules' => 'required|max_length[100]|xss_clean'
                                )
                           ),
        'grupos/editar' => array(
                           array(
                                   'field' => 'name',
                                   'label' => 'Nombre',
                                   'rules' => 'required|max_length[20]|xss_clean'
                                ),
                           array(
                                   'field' => 'description',
                                   'label' => 'Descripcion',
                                   'rules' => 'required|max_length[100]|xss_clean'
                                )
                           )
);

?>