<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* IMG config
*
* PHP Version 5.3
*
* @category PHP
* @package Controller
* @author Slawomir Jasinski <slav123@gmail.com>
* @copyright 2012 All Rights Reserved Imagination
* @license Copyright 2012 All Rights Reserved
* @version CVS: <cvs_id>
* @link http://www.spidersoft.com.au
*/
 
    $config['base_path'] = FCPATH . '/img/imgOpciones/';
    $config['base_url'] = '/PKProject/img/imgOpciones/';

 
/* Location: ./system/application/config/img.php */