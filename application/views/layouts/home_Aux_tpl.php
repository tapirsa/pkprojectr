<!DOCTYPE html>
<html lang="es">
<title> Pablo Kosiner</title>

<head>
    <meta charset="utf-8">
    <title>Pablo Kosiner</title>
    <link rel="icon" type="image/png" href="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $this->layout->getDescripcion(); ?>">
    <meta name="keywords" content="<?php echo $this->layout->getKeywords(); ?>" />
    <meta name="author" content="Tapir - Soluciones Informaticas"> 
  
    <!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
    <!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
    <!--script src="js/less-1.3.3.min.js"></script-->
    <!--append ‘#!watch’ to the browser URL, then refresh the page. -->

    <link rel="stylesheet" href="<?php echo base_url('css/jquery-ui.min.css')?>" >
    <link rel="stylesheet" href="<?php echo base_url('css/bootstrap/bootstrap.min.css')?>" >
    <link rel="stylesheet" href="<?php echo base_url('css/bootstrap/micss2.css')?>" >
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fullpage/jquery.fullPage.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/demokosiner.css')?>" />
    <?php echo $this->layout->css; ?> 

   
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

    
    <script type="text/javascript" src="<?php echo base_url('js/jquery-1.11.2.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/jquery-ui.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/bootstrap/bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/fullpage/jquery.fullPage.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/jquery.contenthover.min.js')?>"></script>




    <?php echo $this->layout->js; ?>   

    <style type="text/css">    

          
        
         
    </style> 

    <script type="text/javascript">
        $(document).ready(function() {
            $('#fullpage').fullpage({
                anchors: ['firstPage', 'secondPage', '3rdPage'],
                verticalCentered: true,
                sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE'],
                navigation: true,
                navigationPosition: 'left',
                navigationTooltips: ['First page', 'Second page', 'Third and last page'],
                afterRender: function(){
                    //playing the video
                    $('video').get(0).play();
                }
            });


            $('.d3').contenthover({
                //overlay_width:270,
                //overlay_height:180,
                effect:'slide',
                slide_direction:'right',
                overlay_x_position:'right',
                overlay_y_position:'center',
                overlay_background:'#000',
                overlay_opacity:0.8
            });

            $("#noticiaDialogPop1").dialog({
                    autoOpen: false,
                    hide: 'fold',
                    show: 'drop'
            });

            $(".demo").on('click', function(){

                    

                    $("#noticiaDialogPop1").dialog('open');

            });




  
        });
    </script>
</head>
<body>
    <!-- CABECERA -->
    <div id="cabecera">
        <div class="container top-cabecera" >
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="navbar-inner">
                            <div class="container">
                                <div class="navbar-header">                    
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> 
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>                   
                                   <a class="navbar-brand" href="#"><?php echo $titulo;?></a>
                                   <div class="visible-xs"><?php echo redesSociales();?></div>
                               </div>
                        
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav">
                                           <?php echo my_get_menu('h');?>
                                    </ul>                                                                
                                    <div class="hidden-xs"><?php echo redesSociales();?></div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>

        </div>
    </div>   

    <!-- CENTRAL -->
    <div id="cuerpo">
        <?php echo  $content_for_layout;?>
    </div>
    <!-- PIE -->
    <div id="pie">
        
    </div>
    <div id="fin">
        <p style="margin-left:10px">© 2014· Tapir Soluciones Informáticas - info@tapir.com.ar</p>
    </div>
</body>
</html>