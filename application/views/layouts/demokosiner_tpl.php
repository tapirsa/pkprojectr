<!DOCTYPE html>
<html lang="es">
<title> Tapir Soluciones Informáticas</title>
<head>
  <meta charset="utf-8">
  <title>Tapir SI</title>
  <link rel="icon" type="image/png" href="<?php echo base_url('img/imgPortada/tapirfavicon.jpg')?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="<?php echo $this->layout->getDescripcion(); ?>">
  <meta name="keywords" content="<?php echo $this->layout->getKeywords(); ?>" />
  <meta name="author" content=""> 
  

    <!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
    <!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
    <!--script src="js/less-1.3.3.min.js"></script-->
    <!--append ‘#!watch’ to the browser URL, then refresh the page. -->
    
   
     <link href="<?php echo base_url('css/bootstrap/bootstrap.min.css')?>" rel="stylesheet">
     <link href="<?php echo base_url('css/bootstrap/micss2.css')?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fullpage/jquery.fullPage.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/demokosiner.css')?>" />
    <?php echo $this->layout->css; ?> 


  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

    
    <script type="text/javascript" src="<?php echo base_url('js/jquery-1.11.2.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/bootstrap/bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/fullpage/jquery.fullPage.min.js')?>"></script>
    <?php echo $this->layout->js; ?>

    <script type="text/javascript">
    $(document).ready(function() {
      $('#fullpage').fullpage({
        anchors: ['firstPage', 'secondPage', '3rdPage'],
        verticalCentered: true,
        sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE'],
        navigation: true,
        navigationPosition: 'left',
        navigationTooltips: ['First page', 'Second page', 'Third and last page'],

        afterRender: function(){
          //playing the video
          $('video').get(0).play();
        }
      });
    });
  </script>
</head>
<body>
    <!-- CABECERA -->
    <div id="cabecera">
        <div class="container top-cabecera" >
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <nav class="navbar navbar-inverse navbar-fixed-top " role="navigation">
                        <div class="navbar-inner">
                            <div class="container">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> 
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button> 
                                    <a class="navbar-brand" href="#"><?php echo $titulo;?></a>
                                    <div class="visible-xs"><?php echo redesSociales();?></div>
                                </div>

                               <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                   <ul class="nav navbar-nav">
                                       <?php echo my_get_menu('h');?>                                
                                   </ul>
                                   <div class="hidden-xs"><?php echo redesSociales();?></div>                                   
                               </div>  
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>   
    <!-- CENTRAL -->
    <div id="cuerpo">
      <?php echo $content_for_layout;?>
    </div>
    <!-- PIE -->
    <div id="pie">
        <div class="container">
            <div class="row " >
                <div class="col-xs-12 col-sm-6 col-md-4 "> </div>
                <div class="col-xs-12 col-sm-12 col-md-4" align="center" >
                   <div class="row ">
                        <div class="col-xs-12 ">
                            <img src="<?php echo base_url('img/imgPortada/tapir.jpg')?>" class="img-circle imgStyloCiculo img-responsive" class="img-circle">
                        </div>
                        <div class="col-xs-12 ">
                             <address> <strong>Tapir Soluciones Informáticas</strong>
							  <br>(0387)4439779 -4140631 -6051635
							 
							 <br> Salta - Argentina -CP 4400<br> <abbr title="Phone"></abbr></address>
                        </div>
                        
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 "> </div>
            </div>
        </div>
    </div>
    <div id="fin">
        <p style="margin-left:10px">© 2014· Tapir Soluciones Informáticas - info@tapir.com.ar</p>
    </div>
</body>
</html>