</style>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="text-info">Noticias <?php echo anchor('noticias/index','Todas',array('class'=>'btn btn-primary'));?></h4>
    </div>
    <div class="panel-body">        


        <div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="text-info">Portada</h4>
    </div>

    <div class="panel-body">
        <!--Este es el panel de noticias de portada-->
        <div class="row">
            <?php foreach ($noticias as $registro) {
                $urlImg=base_url("img/imgN")."/".$registro->imgN ?>
                <div class="col-xs-12 col-sm-4">
                    <div class="panel panel-success">
                        <div class="panel-heading"><?php echo anchor('noticias/index','Quitar',array('class'=>'btn btn-warning btn-xs'));?></div>
                        
                        <div class="panel-footer">
                            <table>
                                <tr>
                                    <td width="30%"><img src="<?php echo $urlImg ?>" class="img-rounded img-responsive" ></img></td>
                                    <td>
                                        <div style="padding-left:5px;" >
                                            <div style="float:right;padding-top: 5px;" >Titulo:<b><?php echo $registro->tituloN ?></b></div>
                                        </div>
                                    </td>
                                </tr>                                
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="panel panel-success">
                        
                        
                        <div class="panel-footer">
                            <table>
                                <tr>
                                    <td width="30%"><img src="<?php echo $urlImg ?>" class="img-rounded img-responsive" ></img></td>
                                    <td>
                                        <div style="padding-left:5px;" >
                                            Titulo:<b><?php echo $registro->tituloN ?></b>
                                        </div>
                                    </td>
                                </tr>                                
                            </table>
                        </div>
                        <div class="panel-heading"><?php echo anchor('noticias/index','Quitar',array('class'=>'btn btn-warning btn-xs'));?></div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">Titulo:<b><?php echo $registro->tituloN ?></b></div>
                        
                        <div class="panel-footer">
                            <table>
                                <tr>
                                    <td width="30%"><img src="<?php echo $urlImg ?>" class="img-rounded img-responsive" ></img></td>
                                    <td>
                                        <div style="padding-left:5px;" >
                                            <b><?php echo $registro->copeteN ?></b>
                                        </div>
                                    </td>
                                </tr>
                                <tr>                                    
                                    <td colspan="2">
                                        <div style="float:right;padding-top: 5px;" ><?php echo anchor('noticias/index','Quitar',array('class'=>'btn btn-warning btn-xs'));?></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
</div>
        <!-- Algun Comentario adicional -->
        <p>Aqui debajo se listan las noticias</p>

        <!-- Mensajes de acciones de opciones -->
        <?php echo my_msj_type($ControlMensajeError,$typeAlert);?>
        
        <!-- Opciones -->  
       
        <div class="row clearfix">
            <div class="col-xs-12 column">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="btn btn-default btn-xs navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                              <span class="glyphicon glyphicon-search"></span>
                            </button>
                            <?php echo anchor('noticias/agregar','Nueva Noticia',array('class'=>'btn btn-primary navbar-btn marge-izq-btn-nav'));?>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

                            <?php echo form_open(null,array('class' => 'navbar-form navbar-right','role'=>'search', 'id'=>'form_search_table', 'style'=>"border:0px"))?>    
                                <?php $options = array('tituloN'  => 'Titulo');?>
                                    <div class="form-group">
                                        <?php echo form_dropdown('tBus',$options,'tituloN','class=form-control');?>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <?php echo form_input(array('type'=>'text','class'=>'form-control','name'=>'datoBus','placeholder'=>'Buscar...'))?>
                                            <div class="input-group-btn">
                                                <?php echo form_button(array('type'=>'submit','content'=>'<span class="glyphicon glyphicon-search"></span>','class'=>'btn btn-default'))?>
                                            </div>
                                        </div>
                                    </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        
        
        <!-- fin Opciones -->  
             
        
        <div class='space-white'></div>
        <!-- Grilla -->
        <table class='footable'>
            <thead>
                <tr>
                    <th data-sort-initial='true' data-sort-ignore='true'></th>
                    <th data-sort-ignore='true'>Imagen</th>
                    <th data-sort-ignore='true'>Titulo</th>
                    <th data-hide='phone' data-sort-ignore='true'>Copete</th>               
                    <th data-hide='phone' data-sort-ignore='true'>Acciones</th>                
                </tr>
            </thead>
            <tbody>
               <?php foreach ($noticias as $registro) {?>                   
                    <tr>
                        <td></td>
                        <?php $urlImg=base_url("img/imgN")."/".$registro->imgN ?>
                        <td><img src="<?php echo $urlImg ?>" class="img-responsive" ></img></td>
                        <td><?php echo $registro->tituloN ?></td>
                        <td><?php echo $registro->copeteN ?></td>                       
                        <td>
                            <?php echo anchor('noticias/editar/'.$registro->id_noticia,'<span class="glyphicon glyphicon-pencil"></span>',array('title'=>'Editar','class'=>'btn btn-default'))?>                            
                            <?php echo anchor('noticias/deshabilitar/'.$registro->id_noticia,'<span class="glyphicon glyphicon-remove-circle"></span>',array('title'=>'Deshavilitar','class'=>'btn btn-default'))?>
                        </td>
                    </tr>
                <?php }?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="12">
                        <?php echo $pagination;?>
                    </td>
                </tr>
            </tfoot>
        </table>
        <div class='space-white'></div>

        <!-- Opciones end grilla --> 
        <div class='form-group'><?php echo anchor('noticias/agregar','Nueva Noticia',array('class'=>'btn btn-primary'));?></div>       
    </div>  
        
</div>

