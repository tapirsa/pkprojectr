<div class="panel panel-default">
    <div class="panel-heading"><h4 class="text-info">Nueva Noticia</h4></div>

    <div class="panel-body">
        <!-- Algun Comentario adicional -->
        <p>Por favor ingrese la informacion de la Noticia.</p>

        <!-- Formulario -->
        <div class="row clearfix">
            <div class=" col-sm-2  column"> </div>
            <div class="col-xs-12 col-sm-8 column">
                <div class="well well-small">

                    <?php echo my_msj_type($ControlMensajeError,$typeAlert);?>                    

                    <?php echo  form_open_multipart(null,array('class'=>'form-horizontal','id'=>'form_create_noticia'));?>

                    <div class="form-group">
                        <?php echo form_label('Titulo','tituloN', array('class'=>'col-sm-12'));?>
                        <div class="col-sm-12">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'tituloN','id'=>'tituloN','placeholder'=>'Titulo...', 'value'=>set_value('tituloN')));?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Copete','copeteN',array('class'=>'col-sm-12'));?>
                        <div class="col-sm-12">
                            <?php echo form_textarea(array('id'=>'copeteN','name'=>'copeteN','class'=>'form-control', 'value'=>set_value('copeteN'))) ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Noticia','descN',array('class'=>'col-sm-12'));?>
                        <div class="col-sm-12">
                            <?php echo form_textarea(array('id'=>'descN','name'=>'descN','class'=>'form-control', 'value'=>set_value('descN'))) ?>
                        </div>
                    </div>               

                    <div class="form-group">
                        <?php echo form_label('Seleccione Imagen','imputImg',array('class'=>'col-sm-12'));?>
                        <div class="col-sm-12">
                            <?php echo form_input(array('type'=>'file','name'=>'files','id'=>'files'));?>
                        </div>                   
                    </div>                    

                    <div class="form-group">
                        <div class='row'>                            
                            <div class="col-xs-6">
                                <?php echo form_label('Img Actual','imgActual', array('class'=>'col-xs-12'));?>
                                <div class="col-xs-12">
                                    <?php $urlImg=base_url('img/imgN/defaultN.png');?>
                                    <output id="list"><img id="imgActual" src="<?php echo $urlImg ?>" class="img-responsive img-rounded"></img></output>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <?php echo form_label('Portada','portadaN',array('class'=>'col-xs-12'));?>
                                <div class="col-xs-12">
                                    <?php echo form_dropdown('portadaN',array('0'=>'NO','1'=>'SI'),0,'class="form-control" onChange="habilitar($(this).val())"');?>
                                </div>                            
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class='col-md-offset-3 col-md-8 btn-group'>
                            <?php echo form_button(array('type'=>'submit','content'=>'Crear','class'=>'btn btn-primary'));?>
                            <?php echo anchor('noticias/index','Cancelar',array('class'=>'btn btn-default'));?>
                        </div>
                    </div>

                    <?php echo form_close();?>
                </div>       
            </div>
            <div class=" col-sm-2  column"></div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('js/visualizarImgArchivo.js')?>"></script>