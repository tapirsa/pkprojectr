<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="text-info">Contactos <?php echo anchor('contactos/index','Todos',array('class'=>'btn btn-primary'));?></h4>
    </div>

    <div class="panel-body">

        <!-- Algun Comentario adicional -->
        <p>Aqui debajo se listan las consultas</p>     

        <!-- Mensajes de acciones de opciones --> 
        <?php echo my_msj_type($ControlMensajeError,$typeAlert);?>    

        <!-- Opciones -->   
        
        <div class="row clearfix">
            <div class="col-xs-12 column">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="btn btn-default btn-xs navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                              <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                            <div class='navbar-form navbar-right'>
                                <label>Filtro:</label>
                                <?php echo form_input(array('name'=>'filter','id'=>'filter','class'=>'form-control','type'=>'text'));?>
                            </div>

                        </div>
                    </div>
                </nav>
            </div>
        </div>
      
        <!-- Grilla --> 
        <table class='footable' data-filter='#filter'>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Autor</th>
                    <th data-hide='phone'>Subject</th>
                    <th data-hide='phone'>mensaje</th>
                    <th data-hide='all'>Email</th>
                    <th data-hide='all'>Teléfono</th>                 
                    <th data-hide='phone'>Fecha</th>
                    <th data-hide='all'> Estado de la Consulta</th>   
                    <th data-hide='phone' data-sort-ignore="true" style="width: 156px;">Acciones</th>   
                                 
                </tr>
            </thead>
            <tbody>
                <?php foreach ($contactos as $contacto):?>
                    <tr>
                        <td><?php echo $contacto->id_contacto?></td>
                        <td><?php echo $contacto->first_name.' '.$contacto->last_name?></td>
                        <td><?php echo $contacto->subject?></td>
                        <td><?php echo $contacto->mensaje?></td>
                        <td><?php echo $contacto->email?></td>
                        
                        <td><?php echo $contacto->phone?></td>

                        <td data-value="<?php echo date('Ymd',strtotime($contacto->created))?>"><?php echo date('d/m/Y',strtotime($contacto->created))?></td>
                        <td><?php echo $contacto->was_answered?></td>
                        <td >
                            <?php echo anchor("contactos/responder/".$contacto->id_contacto,'<span class="glyphicon glyphicon-upload"></span>',array('title'=>'Responder','class'=>'btn btn-default'))?>
                            <?php echo anchor("contactos/deshabilitar/".$contacto->id_contacto, '<span class="glyphicon glyphicon-remove-circle"></span>',array('title'=>'Deshabilitar','class'=>'btn btn-default')) ;?>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                 <tr>
                     <td colspan="8">
                         <div class="pagination pagination-centered hide-if-no-paging"></div>
                     </td>
                 </tr>
            </tfoot>  
        </table>
    </div>
</div>   