<script type="text/javascript">iniciarTinyMCERepuesta()</script> 
<div class="panel panel-default">
    <div class="panel-heading"><h3 class="text-info">Responder Consulta</h3></div>

    <div class="panel-body">
        <!-- Algun Comentario adicional -->
        <p>Por favor ingrese la respuesta a la consulta:</p>

        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="alert alert-success" role="alert">
                    Consultado por: <strong> <?php echo $contacto->first_name.' '.$contacto->last_name?></strong> <br>
                    Consulta: <?php echo $contacto->mensaje;?>
                </div>
                
                <!-- Formulario --> 
                <div class="well well-small">
                    <!-- Mensajes de validacion -->
                    <?php echo my_msj_type($message_error,$typeAlert);?> 

                    <?php echo form_open(null,array('class'=>'form-horizontal','role'=>'form','id'=>'form_responder_contacto'));?>
                        <div class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                <?php echo form_label('Repuesta','name', array('class'=>'control-label'));?>
                                <textarea  name="respuesta" id="respuesta" >
                                    <?php echo set_value('name',$contacto->respuesta); ?>
                                </textarea>
                            </div>
                        </div>
                        <?php
                             $rdoGuardar = array('type'=>"radio", 'name'=>"optResp", 'value'=>"1"); 
                            if($contacto->was_answered == 1 || $contacto->was_answered == 3)
                                $rdoGuardar['checked'] = "checked"; 

                            $rdoGardarEnviar = array('type'=>"radio", 'name'=>"optResp", 'value'=>"2"); 
                            if($contacto->was_answered == 2)
                                $rdoGardarEnviar['checked'] = "checked";
                        ?>
                    
                        <div class="form-group">
                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Opcion</label>
                            <div class=" col-sm-7 col-md-7 col-lg-7">
                                <div class="radio">
                                    <label>
                                        <?php echo form_input($rdoGuardar)?> Guardar
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <?php echo form_input($rdoGardarEnviar)?> Guardar y Enviar
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-offset-5 col-sm-7 col-md-offset-5 col-md-7 btn-group">
                              <?php echo form_button(array('type'=>'submit','content'=>'OK','class'=>'btn btn-primary'));?>
                              <?php echo anchor('contactos/index','Cancelar',array('class'=>'btn btn-default'));?>
                          </div>
                        </div>
                        <?php echo form_hidden('id_contacto',$contacto->id_contacto);?>
                        <?php echo form_hidden('email',$contacto->email);?>
                        <?php echo form_hidden('id_respuesta',$contacto->id_respuesta);?>
   
                    <?php echo form_close();?>
                </div>  
            </div>
        </div>
    </div>
</div>
   
    