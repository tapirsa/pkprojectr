<div class="panel panel-default">
    <div class="panel-heading"><h4 class="text-info">Editar Grupo</h4></div>

    <div class="panel-body">
        <!-- Algun Comentario adicional -->
        <p>Por favor ingrese la informacion del grupo.</p>

        <!-- Formulario -->

        <div class="row clearfix">
            <div class="col-sm-2 col-md-2 column"> </div>
            <div class="col-xs-12 col-sm-8 col-md-8 column">
                <div class="well well-small">  
                    <?php echo my_msj_type($ControlMensajeError,$typeAlert);?>

                    <?php echo form_open(null,array('class'=>'form-horizontal','id'=>'form_create_grupo'));?>    

                        <div class="form-group">
                            <?php echo form_label('Id','id', array('class'=>'col-sm-3 control-label'));?> 
                            <div class="col-sm-8">
                                <p class="form-control-static"><?php echo $registro->id?></p>
                                <?php echo form_hidden('id',$registro->id);?>
                            </div>
                        </div>

                        <div class="form-group">
                          <?php echo form_label('Nombre','name', array('class'=>'col-sm-3 control-label'));?>
                          <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'name','id'=>'name','placeholder'=>'Nombre...', 'value'=>set_value('name',$registro->name)));?>
                          </div>
                        </div>

                        <div class="form-group">
                          <?php echo form_label('Descripcion','description', array('class'=>'col-sm-3 control-label'));?>
                          <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'description','id'=>'description','placeholder'=>'Descripcion...','value'=>set_value('description',$registro->description)));?>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-md-offset-3 col-md-8 btn-group">
                              <?php echo form_button(array('type'=>'submit','content'=>'Editar','class'=>'btn btn-primary'));?>
                              <?php echo anchor('grupos/index','Cancelar',array('class'=>'btn btn-default'));?>
                          </div>
                        </div>

                    <?php echo form_close();?>
                </div>
            </div>
            <div class="col-sm-2 col-md-2 column"></div>
        </div>
    </div>
</div>
    
    