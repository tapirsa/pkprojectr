    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="text-info">Grupos <?php echo anchor('grupos/index','Todos',array('class'=>'btn btn-primary'));?> </h4>
        </div>

        <div class="panel-body">
            
            <!-- Algun Comentario adicional -->
            <p>Aqui debajo se listan los grupos</p>     

            <!-- Mensajes de acciones de opciones --> 
            <?php echo my_msj_type($ControlMensajeError,$typeAlert);?>    

            <!-- Opciones -->   
            <div class="row clearfix">
                <div class="col-xs-12 column">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                                <span class="glyphicon glyphicon-search"></span>
                              </button>
                              <?php echo anchor('grupos/agregar','Crear Grupo',array('class'=>'btn btn-primary navbar-btn marge-izq-btn-nav'));?>
                            </div>
                            
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                                    <?php echo form_open(null,array('class' => 'navbar-form navbar-right','role'=>'search', 'id'=>'form_search_table', 'style'=>"border:0px"))?>    
                                    <?php $options = array('name'  => 'Nombre','description' => 'Descripcion');?>
                                        <div class="form-group">
                                            <?php echo form_dropdown('tBus',$options,'name','class=form-control');?>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group">
                                                <?php echo form_input(array('type'=>'text','class'=>'form-control','name'=>'datoBus','placeholder'=>'Buscar...'))?>
                                                <div class="input-group-btn">
                                                    <?php echo form_button(array('type'=>'submit','content'=>'<span class="glyphicon glyphicon-search"></span>','class'=>'btn btn-default'))?>
                                                </div>
                                            </div>
                                        </div>
                                <?php echo form_close();?>
                                    
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            
            <!-- Grilla --> 
            <table class='footable'>
                <thead>
                    <tr>
                        <th data-sort-initial='true'>Id</th>
                        <th>Nombre</th>
                        <th data-hide='phone' data-sort-ignore='true'>Descripcion</th>
                        <th data-hide='phone' data-sort-ignore='true'>Creado</th>
                        <th data-hide='phone' data-sort-ignore='true'>Actualizado</th>
                        <th data-hide='phone' data-sort-ignore='true'>Acciones</th>                
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($grupos as $grupo):?>
                        <tr>
                            <td><?php echo $grupo->id?></td>
                            <td><?php echo $grupo->name?></td>
                            <td><?php echo $grupo->description?></td>
                            <td><?php echo date('d/m/Y',  strtotime($grupo->created))?></td>
                            <td><?php echo date('d/m/Y',  strtotime($grupo->updated))?></td>
                            <td>
                                <?php echo anchor("grupos/editar/".$grupo->id,'<span class="glyphicon glyphicon-pencil"></span>',array('title'=>'Editar','class'=>'btn btn-default'))?>
                                <?php echo anchor("grupos/deshabilitar/".$grupo->id, '<span class="glyphicon glyphicon-remove-circle"></span>',array('title'=>'Deshabilitar','class'=>'btn btn-default')) ;?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="12">
                            <?php echo $pagination;?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>   