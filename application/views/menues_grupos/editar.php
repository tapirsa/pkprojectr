<div class="panel panel-default">
    <div class="panel-heading"><h4 class="text-info">Editar acceso de Menu</h4></div>

    <div class="panel-body">
        <!-- Algun Comentario adicional -->
        <p>Por favor ingrese la informacion del grupo para el usuarios.</p>

        <!-- Formulario --> 
        <div class="row clearfix">
            <div class="col-sm-2 col-md-2 column"> </div>
            <div class="col-xs-12 col-sm-8 col-md-8 column">
                 <div class="well well-small">
                    <?php echo  my_msj_type($ControlMensajeError,$typeAlert);?>
                    <?php echo form_open(null,array('class'=>'form-horizontal'));?>
                        <div class="form-group">
                            <?php echo form_label('id','id_menu_group', array('class'=>'col-sm-3 control-label'));?> 
                            <div class="col-sm-8">
                                <p class="form-control-static"><?php echo $menues_grupos->id_menu_group;?></p>
                                <?php echo form_hidden('id_menu_group',$menues_grupos->id_menu_group);?>      
                            </div>
                        </div>
                     
                        <div class="form-group">
                            <?php echo form_label('Grupo','id_group',array('class'=>'col-sm-3 control-label'));?>
                            <div class="col-sm-8">
                                <?php echo form_dropdown('id_group',$opciones_grupo,$menues_grupos->id_group,"class='form-control'");?>
                            </div>
                        </div>
                     
                        <div class="form-group">
                          <?php echo form_label('Menu','id_menu',array('class'=>'col-sm-3 control-label'));?>
                          <div class="col-sm-8">
                           <?php echo form_dropdown('id_menu',$opciones_menu,$menues_grupos->id_menu,"class='form-control'");?>
                          </div>
                        </div>
                     
                        <div class="form-group">
                          <div class="col-md-offset-3 col-md-8 btn-group">
                            <?php echo form_button(array('type'=>'submit','content'=>'Editar','class'=>'btn btn-primary'));?>
                            <?php echo anchor('menues_grupos/index','Cancelar',array('class'=>'btn btn-default'));?>
                          </div>
                        </div>
                     
                    <?php echo form_close();?>
                </div> 
            </div>
            <div class="col-sm-2 col-md-2 column"></div>
        </div>
    </div>
</div>
    
    