<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="text-info"> Menues por Grupos <?php echo anchor('menues_grupos/index','Todos',array('class'=>'btn btn-primary'));?></h4>
    </div>
    
    <div class="panel-body">
        
    </div>

    <div class="panel-body">
       <!-- Algun Comentario adicional -->
        <p>Aqui debajo se listan los accesos</p>

        <!-- Mensajes de acciones de opciones -->
        <?php echo my_msj_type($ControlMensajeError,$typeAlert);?>

        <!-- Opciones -->     
         <div class="row clearfix">
            <div class="col-xs-12 column">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                            <span class="glyphicon glyphicon-search"></span>
                          </button>
                          <?php echo anchor('menues_grupos/agregar','Crear acceso a Menu',array('class'=>'btn btn-primary navbar-btn marge-izq-btn-nav'));?>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                            <?php echo form_open(null,array('class' => 'navbar-form navbar-right','role'=>'search', 'id'=>'form_search_table', 'style'=>"border:0px"))?>    
                                <?php $options = array('g.name'  => 'Grupo','m.name'  => 'Menu');?>
                                <div class="form-group">
                                    <?php echo form_dropdown('tBus',$options,'name','class=form-control');?>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <?php echo form_input(array('type'=>'text','class'=>'form-control','name'=>'datoBus','placeholder'=>'Buscar...'))?>
                                        <div class="input-group-btn">
                                            <?php echo form_button(array('type'=>'submit','content'=>'<span class="glyphicon glyphicon-search"></span>','class'=>'btn btn-default'))?>
                                        </div>
                                    </div>
                                </div>
                            <?php echo form_close();?>

                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <table class='footable'>
            <thead>
                <tr>
                    <th data-sort-initial='true'>Id</th>
                    <th>Grupo</th>
                    <th data-hide='phone' data-sort-ignore='true'>Menu</th>
                    <th data-hide='phone' data-sort-ignore='true'>Creado</th>
                    <th data-hide='phone' data-sort-ignore='true'>Actualizado</th>
                    <th data-hide='phone' data-sort-ignore='true'>Accion</th>               
                </tr>
            </thead>
            <tbody>
               <?php foreach ($menues_grupos as $registro) {?>
                    <tr>
                        <td><?php echo $registro->id_menu_group ;?></td>
                        <td><?php echo $registro->name_grupo ;?></td>
                        <td><?php echo $registro->name_menu ;?></td>
                        <td><?php echo date('d/m/Y',strtotime($registro->created)) ;?></td>
                        <td><?php echo date('d/m/Y',strtotime($registro->updated)) ;?></td>
                        <td>
                            <?php echo anchor('menues_grupos/editar/'.$registro->id_menu_group,'<span class="glyphicon glyphicon-pencil"></span>',array('title'=>'Editar','class'=>'btn btn-default'));?>
                            <?php echo anchor('menues_grupos/deshabilitar/'.$registro->id_menu_group,'<span class="glyphicon glyphicon-remove-circle"></span>',array('title'=>'Deshabilitar','class'=>'btn btn-default'));?>
                        </td>
                    </tr>
                <?php }?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan='6'>
                        <?php echo $pagination ?>
                    </td>
                </tr>
            </tfoot>
        </table> 
    </div>
</div>

    