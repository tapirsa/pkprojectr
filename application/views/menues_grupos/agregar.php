<div class="panel panel-default">
    <div class="panel-heading"><h4 class="text-info">Crear acceso de Menu</h4></div>

    <div class="panel-body">
        <!-- Algun Comentario adicional -->
        <p>Por favor ingrese la informacion del grupo para el usuarios.</p>

        <div class="row clearfix">
            <div class="col-sm-2 col-md-2 column"> </div>
            <div class="col-xs-12 col-sm-8 col-md-8 column">
                <div class="well well-small">

                    <?php echo my_msj_type($ControlMensajeError,$typeAlert);?>

                    <?php echo form_open(null,array('class'=>'form-horizontal'));?> 

                        <div class="form-group">
                          <?php echo form_label('Grupo','id_group',array('class'=>'col-sm-3 control-label'));?> 
                          <div class="col-sm-8">
                           <?php echo form_dropdown('id_group',$opciones_grupo,0,"class='form-control'");?> 
                          </div>
                        </div>

                        <div class="form-group">
                          <?php echo form_label('Menu','id_menu',array('class'=>'col-sm-3 control-label'));?> 
                          <div class="col-sm-8">
                              <?php echo form_dropdown('id_menu',$opciones_menu,0,"class='form-control'");?> 
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-md-offset-3 col-md-8 btn-group">
                            <?php echo form_button(array('type'=>'submit','content'=>'Crear','class'=>'btn btn-primary'));?> 
                            <?php echo anchor('menues_grupos/index','Cancelar',array('class'=>'btn btn-default'));?> 
                          </div>
                        </div>

                    <?php echo form_close();?>
                </div>   
            </div>
            <div class="col-sm-2 col-md-2 column"></div>
        </div>
    </div>
</div>    