<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="text-info" > <?php echo lang('index_heading');?> <?php echo anchor('auth/index','Todos',array('class'=>'btn btn-primary'));?> </h4>
    </div>
    <div class="panel-body">
        <!-- Algun Comentario adicional -->
        <p><?php echo lang('index_subheading');?></p>

        <!-- Mensajes de accion de opciones --> 
        <?php echo my_msj_type($message,$typeAlert);?>

        <!-- Opciones -->   
        <div class="row clearfix">
            <div class="col-xs-12 column">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="btn btn-default btn-xs navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                              <span class="glyphicon glyphicon-search"></span>
                            </button>
                            <div class="btn-group hidden-xs navbar-btn marge-izq-btn-nav">
                                <?php echo anchor('auth/create_user',lang('index_create_user_link'),array('class'=>"btn btn-primary"));?>
                            </div>
                            <div class="btn-group visible-xs navbar-btn marge-izq-btn-nav">
                                <?php echo anchor('auth/create_user',lang('index_create_user_link'),array('class'=>"btn btn-primary btn-xs"));?>
                            </div>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

                            <?php echo form_open(null,array('class' => 'navbar-form navbar-right','role'=>'search', 'id'=>'form_search_table', 'style'=>"border:0px"))?>    
                                <?php $options = array('first_name'=> 'Nombre','last_name'=> 'Apellido', 'username'=> 'Usuario', 'email' => 'email');?>
                                    <div class="form-group">
                                        <?php echo form_dropdown('tBus',$options,$tBus,'class=form-control');?>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <?php echo form_input(array('type'=>'text','class'=>'form-control','name'=>'datoBus','placeholder'=>'Buscar...'))?>
                                            <div class="input-group-btn">
                                                <?php echo form_button(array('type'=>'submit','content'=>'<span class="glyphicon glyphicon-search"></span>','class'=>'btn btn-default'))?>
                                            </div>
                                        </div>
                                    </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        
        <!-- Grilla -->
        <table id="tblusers" class='footable' data-filter='#filter' data-filter-text-only="true">
            <thead>
                <tr>
                    <th data-sort-initial='true'><?php echo lang('index_fname_th');?></th>
                    <th><?php echo lang('index_lname_th');?></th>
                    <th data-hide='phone'><?php echo lang('index_email_th');?></th>
                    <th data-hide='phone' data-sort-ignore='true'><?php echo lang('index_groups_th');?></th>
                    <th data-hide='phone' data-sort-ignore='true'><?php echo lang('index_action_th');?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user):?>
                    <tr>
                        <td><?php echo $user->first_name;?></td>
                        <td><?php echo $user->last_name;?></td>
                        <td><?php echo $user->email;?></td>
                        <td>
                            <?php foreach ($user->groups as $group):?>
                                <?php echo $group->name;?>
                            <?php endforeach?>
                        </td>
                        <td>
                            <?php echo anchor("auth/edit_user/".$user->id,'<span class="glyphicon glyphicon-pencil"></span>',array('title'=>'Editar','class'=>'btn btn-default')) ;?>
                            <?php echo ($user->active) ? anchor("auth/deactivate/".$user->id,'<span class="glyphicon glyphicon-remove-circle"></span>',array('title'=>lang('index_active_link'),'class'=>'btn btn-default')) : 
                                                         anchor("auth/activate/". $user->id,'<span class="glyphicon glyphicon-ok-circle"></span>',array('title'=>lang('index_inactive_link'),'class'=>'btn btn-default'));?>
                        </td>

                    </tr>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                 <tr>
                     <td colspan="6">
                         <?php echo $pagination;?>
                     </td>
                 </tr>
             </tfoot>   
        </table>
        
        <div class='space-white'></div>
        <!-- Opciones end grilla --> 
        <div class="btn-group">
            <?php echo anchor('auth/create_user',lang('index_create_user_link'),array('class'=>"btn btn-primary"));?>
        </div>
    </div>
</div>
   
    
