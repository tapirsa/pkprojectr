<div class="panel panel-default">
    <div class="panel-heading"><h3 class="text-info"><?php echo lang('deactivate_heading');?></h3></div>
    <div class="panel-body">
        <p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>

        <div class="row clearfix">
            <div class="col-sm-2 col-md-2 column"></div>
            <div class="col-xs-12 col-sm-8 col-md-8 column">
                <div class="well well-small">
                    <?php echo form_open("auth/deactivate/".$user->id,array('class'=>'form-horizontal'))?>
                    
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="radio-inline">
                                    <label>
                                        <input type="radio" name="confirm" value="yes" checked="checked"> <?php echo lang('deactivate_confirm_y_label')?>
                                    </label>
                                </div>
                                <div class="radio-inline">
                                    <label>
                                        <input type="radio" name="confirm"  value="no" > <?php echo lang('deactivate_confirm_n_label')?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    
                        <div class='form-group'>
                            <div class='col-md-8 btn-group'>
                                <?php echo form_submit(array('value'=>lang('deactivate_submit_btn'),'class'=>'btn btn-primary'));?>
                                <?php echo anchor("auth/index","Cancelar",array('class'=>'btn btn-default'))?>
                            </div>
                        </div>
                        <?php echo form_hidden($csrf); ?>
                        <?php echo form_hidden(array('id'=>$user->id)); ?>
                    <?php echo form_close()?>
                </div>
            </div>
            <div class="col-sm-2 col-md-2 column"></div>
        </div>
    </div>
</div>