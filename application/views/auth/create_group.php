<div class="panel panel-default">
    <div class="panel-heading"><h3 class="text-info"><?php echo lang('create_group_heading');?></h3></div>
    <div class="panel-body">
        <p><?php echo lang('create_group_subheading');?></p>

        <div class="row clearfix">
            <div class="col-sm-2 col-md-2 column"></div>
            <div class="col-xs-12 col-sm-8 col-md-8 column">
                <div class="well well-small">
                    <?php echo my_msj_type($message,$typeAlert)?>
                    <?php echo form_open("auth/create_group",array('id'=>'form_create_grupo','class'=>"form-horizontal"))?>
                    
                        <div class='form-group'>
                            <label class='col-sm-3 control-label' for='group_name'><?php echo lang('create_group_name_label')?></label>
                            <div class='col-sm-8'>
                              <?php echo form_input($group_name)?>
                            </div>
                        </div>

                        <div class='form-group'>
                            <label class='col-sm-3 control-label' for='description'><?php echo lang('create_group_desc_label')?></label>
                            <div class='col-sm-8'>
                              <?php echo form_input($description)?>
                            </div>
                        </div>

                        <div class='form-group'>
                            <div class='col-md-offset-3 col-md-8 btn-group'>
                                <?php echo form_submit(array('value'=>lang('create_group_submit_btn'),'class'=>'btn btn-primary'))?>
                                <?php echo anchor("auth","Cancelar",array('class'=>'btn btn-default'))?>
                            </div>
                        </div>

                    <?php echo form_close()?>
                </div>
            </div>
            <div class="col-sm-2 col-md-2 column"></div>
        </div>
    </div>
</div>    