<div class="row">
	<div class="col-sm-3 col-md-3 col-lg-4" ></div>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
		<div class="panel">
		    <div class="panel-heading background-header-panel header-panel-stylespecific">
				<?php echo lang('reset_password_heading');?>
		    </div>
		    <div class="panel-body">
		    	<div class="row">
		    		<div class="col-xs-12 col-lg-12">
	                    <?php echo my_msj_type($message,$typeAlert);?>

						<?php echo form_open('auth/reset_password/' . $code, 'class="horizontal"');?>

							<div class="form-group">
								<div class="row">
									<div class="col-xs-12 col-lg-12">
										<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label>
										<?php echo form_input($new_password);?>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-xs-12 col-lg-12">
										<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?>
										<?php echo form_input($new_password_confirm);?>
									</div>
								</div>
							</div>

							<?php echo form_input($user_id);?>
							<?php echo form_hidden($csrf); ?>

							<div class='form-group'>
	                            <div class='col-xs-offset-4 col-xs-8 col-md-offset-3 col-md-8 col-lg-offset-4 col-lg-8'>
	                                <?php echo form_submit(array('value'=>lang('reset_password_submit_btn'),'class'=>'btn btn-primary'));?>
	                            </div>
	                        </div>

						<?php echo form_close();?>
					</div>
    	        </div>
		    </div>
		</div>
	</div>
	<div class="col-sm-3 col-md-3 col-lg-4"></div>
</div>



