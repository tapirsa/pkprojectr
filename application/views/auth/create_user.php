    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="text-info"><?php echo lang('create_user_heading');?></h3></div>
        
        <div class="panel-body">
            <p><?php echo lang('create_user_subheading');?></p>
            
            <div class="row clearfix">
                <div class="col-sm-2 col-md-2 column"> </div>
                <div class="col-xs-12 col-sm-8 col-md-8 column">
                    <!-- Formulario --> 
                    <div class="well well-small">
                        <!-- Mensajes de validacion --> 
                        <?php echo my_msj_type($message,$typeAlert);?>
                        <?php echo form_open(null, array('id'=>'form_create_user','role'=>'form','class'=>'form-horizontal'));?>

                            <div class="form-group">
                                <label class='col-sm-3 control-label' for='first_name'><?php echo lang('create_user_fname_label');?></label>
                                <div class="col-sm-8">
                                    <?php echo form_input($first_name);?>
                                </div>
                            </div>    

                            <div class='form-group'>
                                <label class='col-sm-3 control-label' for='last_name'><?php echo lang('create_user_lname_label');?></label>
                                <div class='col-sm-8'>
                                  <?php echo form_input($last_name);?>
                                </div>
                            </div>

                            <div class='form-group'>
                                <label class='col-sm-3 control-label' for='company'><?php echo lang('create_user_company_label');?></label>
                                <div class='col-sm-8'>
                                  <?php echo form_input($company);?>
                                </div>
                            </div>

                            <div class='form-group'>
                                <label class='col-sm-3 control-label' for='email'><?php echo lang('create_user_email_label');?></label>
                                <div class='col-sm-8'>
                                  <?php echo form_input($email);?>
                                </div>
                            </div>

                            <div class='form-group'>
                                <label class='col-sm-3 control-label' for='phone'><?php echo lang('create_user_phone_label');?></label>
                                <div class='col-sm-8'>
                                  <?php echo form_input($phone);?>
                                </div>
                            </div>

                            <div class='form-group'>
                                <label class='col-sm-3 control-label' for='password'><?php echo lang('create_user_password_label');?></label>
                                <div class='col-sm-8'>
                                  <?php echo form_input($password);?>
                                </div>
                            </div>

                            <div class='form-group'>
                                <label class='col-sm-3 control-label' for='password_confirm'><?php echo lang('create_user_password_confirm_label');?></label>
                                <div class='col-sm-8'>
                                  <?php echo form_input($password_confirm);?>
                                </div>
                            </div>

                            <div class='form-group'>
                                <div class='col-md-offset-3 col-md-8 btn-group'>
                                    <?php echo form_submit(array('value'=>lang('create_user_submit_btn'),'class'=>'btn btn-primary'));?>
                                    <?php echo anchor("auth/index","Cancelar",array('class'=>'btn btn-default'));?>
                                </div>
                            </div>

                        <?php echo form_close()?>
                    </div>
                </div>
                <div class="col-sm-2 col-md-2 column"></div>
            </div>
        </div>
    </div>
  
    
   
    