<script type="text/javascript">
    

</script>

<div class="row">
        <div class="col-md-4 column"></div>
        <div class="col-md-4 column">
            <div class="panel">
                <div class="panel-heading background-header-panel header-panel-stylespecific">
                    <?php echo lang('login_heading');?>
                </div>
                <div class="panel-body">
                
                    <p><?php echo lang('login_subheading');?></p>
                    
                    <?php echo my_msj_type($message,$typeAlert);?>
                    
                    <?php echo  form_open("auth/login", array('role'=>"form"));?>
                    
                        <div class="form-group">
                            <label for="identity"><?php echo lang('login_identity_label');?></label>
                            <?php echo form_input($identity);?>
                        </div>
                        <div class="form-group">
                            <label for="password"><?php echo lang('login_password_label');?></label>
                            <?php echo form_input(array('placeholder'=>"Ingresa tu Clave",'class'=>'form-control','name' => 'password','id' => 'password','type' => 'password',));?>
                        </div>
                        <div class="checkbox">
                            <label>
                                <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"')." ".lang('login_remember_label');?>
                            </label>
                        </div> 
                    
                        <?php echo form_submit(array('value'=>lang('login_submit_btn'),'class'=>'btn btn-primary'));?>
                       
                    <?php echo form_close();?>
                    
                    <div class="row">
                        <div class="col-md-12 column">
                            <a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4 column"></div>
</div>





