
    <div class="row">
        <div class="col-sm-3 col-md-3 col-lg-4" ></div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <div class="panel">
                <div class="panel-heading background-header-panel header-panel-stylespecific">
                    <?php echo lang('forgot_password_heading');?>
                </div>
                <div class="panel-body">
                    <!-- Algun Comentario adicional -->
                    <div class="row">
                        <div class="col-lg-12">
                            <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-lg-12">
                            <?php echo my_msj_type($message,$typeAlert);?>
                            <?php echo form_open("auth/forgot_password",array('class'=>"form-horizontal"));?>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label class="control-label" for="email"><?php echo sprintf(lang('forgot_password_email_label'), $identity_label);?></label>
                                        <?php echo form_input($email);?>
                                    </div>
                                </div>

                                <div class='from-group'>
                                    <div class='controls'>
                                      <?php echo form_submit(array('type'=>'submit','value'=>lang('forgot_password_submit_btn'),'class'=>'btn btn-primary'));?>
                                      <?php echo anchor("auth/login","Cancelar",array('class'=>'btn btn-default'));?>
                                    </div>
                                </div>

                            <?php echo form_close();?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-4"></div>
    </div>


    




