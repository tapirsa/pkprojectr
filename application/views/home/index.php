<div id="fullpage">
    <div class="section " id="section0">
        <video autoplay loop muted id="myVideo" class="hidden-xs visible-sm visible-md visible-lg">
            <source src="<?php echo base_url('img/imageneskosiner/flowers.mp4')?>" type="video/mp4">
            <source src="imgs/flowers.webm" type="video/webm">
        </video>
        <div class="layer">
            <h1></h1>
        </div>
    </div>
    <div class="section" id="section1">
        <div class="slide" id="slide1"><h1></h1>
        </div>
        <div class="slide" id="slide2"><h1></h1>
        </div>
    </div>
    <div class="section" id="section2">
        <div class="container">
            <?php echo $noticiasView; ?>
        </div>
    </div>
    <div class="section" id="section3">
        <div class="container">
            <?php echo $galeriaImagenesView; ?>
        </div> 
    </div>
</div>