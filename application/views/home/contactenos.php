<div class="panel">
    <div class="panel-heading background-header-panel header-panel-stylespecific">
      <?php echo lang('contacto_heading');?>
    </div>
    <div class="panel-body">
        <div class="col-md-12">
            <!-- Algun Comentario adicional -->
            <p><?php echo lang('contacto_subheading');?></p>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 column">
                    <div class="well well-small">
                        <?php echo my_msj_type($message,$typeAlert);?>

                        <?php echo form_open(null, array('role'=>'form','id'=>'form_agregar_contacto','class'=>"form-horizontal"));?>
                                <div class="form-group">
                                    <label class='col-xs-offset-1 col-md-offset-4 control-label'><?php echo lang('contacto_msj_required_label');?></label>
                                </div>

                                <div class="form-group">
                                   <label class='control-label col-md-3' for='first_name'><?php echo lang('contacto_nombre_label');?></label>
                                   <div class="col-md-9">      
                                        <?php echo form_input(array('name' =>'first_name','id'=>'first_name','class'=>'form-control','type'=>'text',
                                                                    'placeholder'=>lang('contacto_nombre_placeholder'),
                                                                    'value'=>set_value('first_name',$contacto['first_name'])));?>
                                   </div>
                                </div>

                                <div class='form-group'>
                                    <label class='col-md-3 control-label' for='last_name'><?php echo lang('contacto_apellido_label');?></label>
                                    <div class='col-md-9 controls'>
                                      <?php echo form_input(array('name'=>'last_name','id'=>'last_name','class'=>'form-control','type'=>'text',
                                                                  'placeholder' => lang('contacto_apellido_placeholder'),
                                                                  'value'=>set_value('last_name',$contacto['last_name'])));?>
                                    </div>
                                </div>

                                <div class="form-group">
                                   <label class="col-md-3 control-label" for="email"><?php echo lang('contacto_email_label');?></label>
                                   <div class="col-md-9 controls">
                                       <?php echo form_input(array( 'name'=>'email','id'=>'email','class'=>'form-control','type'=>'text',
                                                                    'placeholder' => lang('contacto_email_placeholder'),
                                                                    'value'=>set_value('email',$contacto['email'])));?>
                                   </div>
                               </div>

                                <div class='form-group'>
                                    <label class='col-md-3 control-label' for='phone'><?php echo lang('contacto_phone_label');?></label>
                                    <div class='col-md-9 controls'>
                                      <?php echo form_input(array('name'=>'phone','id'=>'phone','class'=>'form-control','type'=>'text',
                                                                  'placeholder' => lang('contacto_phone_placeholder'),
                                                                  'value'=>set_value('phone',$contacto['phone'])));?>
                                    </div>
                                </div>

                                <div class='form-group'>
                                    <label class='col-md-3 control-label' for='subject'><?php echo lang('contacto_subject_label');?></label>
                                    <div class='col-md-9 controls'>
                                      <?php echo form_input(array('name'=>'subject','id'=>'subject','class'=>'form-control','type'=>'text',
                                                                  'placeholder' => lang('contacto_subject_placeholder'),
                                                                  'value' =>set_value('subject',$contacto['subject'])));?>
                                    </div>
                                </div>

                                <div class='form-group'>
                                    <label class='col-md-3 control-label' for='mensaje'><?php echo lang('contacto_mensaje_label');?></label>
                                    <div class=' col-md-9 controls'>
                                      <?php echo form_textarea(array('name'=>'mensaje','id'=>'mensaje','class'=>'form-control',
                                                                     'placeholder' => lang('contacto_mensaje_placeholder'),
                                                                     'value' => set_value('mensaje',$contacto['mensaje'])));?>
                                    </div>
                                </div>

                                <div class='form-group'>
                                  <label class='col-md-3 control-label' for='mensaje'>¿Qué ves?</label>
                                  <div class="col-md-9">
                                    <div class="row">
                                      <div class="col-xs-5 col-md-6 col-lg-4">
                                        <?php echo $captcha['image'] ?>
                                      </div>
                                      <div class="col-xs-7 col-md-6 col-lg-8">
                                          <?php echo form_input(array('name'=>'captcha','id'=>'captcha','class'=>'form-control','type'=>'text',
                                                                      'placeholder' => 'caracteres de la imagen'));?>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class='form-group'>
                                    <div class='col-md-offset-3 col-md-8'>
                                        <?php echo form_submit(array('value'=>lang('enviar_contacto_submit_btn'),'class'=>'btn btn-primary'));?>
                                    </div>
                                </div>

                        <?php echo form_close();?>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 column">
                  <div class=" hidden-xs">
                      <p style="font-family: fantasy;font-size: 24px;text-align: center;">Tapir - Soluciones Informáticas</p>
                      <div align="center">
                          <img src="<?php echo base_url('img/imgPortada/tapirLogo.jpg')?>" class="img-circle img-responsive">
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>




