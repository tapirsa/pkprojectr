<script type="text/javascript">
    
    tinymce.init({
    selector: "textarea#txtaNosotros",
    theme: "modern",
    statusbar: true,// barra del final
    menubar : false,
    language : 'es', 
//    width: 300,
    height: 700,
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "newdocument insertfile | undo | redo | styleselect | formatselect | fontselect | fontsizeselect | bold | italic | underline | strikethrough | alignleft | aligncenter | alignright | alignjustify | \n\
            bullist | numlist | outdent | indent | link | image | media | forecolor | backcolor"
//   style_formats: [
//        {title: 'Bold text', inline: 'b'},
//        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
//        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
//        {title: 'Example 1', inline: 'span', classes: 'example1'},
//        {title: 'Example 2', inline: 'span', classes: 'example2'},
//        {title: 'Table styles'},
//        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
//    ]
 }); 

</script>

<legend><?=lang('home_nosotros_heading')?></legend>

<p><?=lang('home_nosotros_subheading')?></p>

<div class="container-fluid">
    
    <div class="row-fluid">
        <div class="span12">
            <div class="well">
            <p>
                <?=  lang('home_nosotros_desarrollo')?>
            </p>
            </div>
        </div>
    </div>
    
    <div class="row-fluid">
        <div class="span10">
            <div class="well">
                <?= form_textarea(array('id'=>'txtaNosotros','name'=>'txtaNosotros')) ?>
            </div>
        </div>
    </div>
</div>
   