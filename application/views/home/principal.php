<div class="row clearfix">
	<div class="col-md-12 column">
        <div class="row">
			<?php $opciones=my_menu_opUsu();
			foreach($opciones as $opcion){
				$accion=base_url($opcion['accion']);
				$img=base_url('img/imgOpciones/253x148/'.$opcion['img']);?>
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 hidden-xs capaOpGlobal" onclick="location.href='<?php echo $accion?>'">
					<div class="thumbnail capaOpPpal" >
						<img title="<?php echo $opcion["decripcionOp"]?>" src="<?php echo $img?>"/>
						<div class="caption">
							<h3><?php echo $opcion["nombre"]?></h3>					
						</div>
					</div>
				</div>	
			<?php } ?>
			<div class="col-xs-12 visible-xs">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-decoration:none;background-color:#428bca">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="text-decoration:none;color:#FFFFFF">
                                Operaciones
                                <div style="float:right">
                                    <span class="glyphicon glyphicon-list" ></span>
                                </div>
                            </a>                               
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse"  >
                            <div class="panel-body" style="padding:0px">                                
                                <?php echo my_get_menu('v');?>  
                            </div>
                        </div>                            
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>