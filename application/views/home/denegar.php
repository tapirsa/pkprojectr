<div class="panel panel-default">
	<div class="panel-body">
		 <div class="row">
            <div class="col-xs-12 column">
    			<?php echo my_msj_type($ControlMensajeError,$typeAlert);?>    			
    		</div>
    	</div>
    	<div class="row">
            <div class="col-xs-12 column">
    			<?php echo anchor($urlBack, 'Volver', 'title="Volver a pagina anterior." class="btn btn-primary"'); ?>    	
    		</div>
    	</div>	
    </div>
</div>