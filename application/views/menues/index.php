<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="text-info">Menues <?php echo anchor('menues/index','Todos',array('class'=>'btn btn-primary'));?></h4>
    </div>

    <div class="panel-body">
        <!-- Algun Comentario adicional -->
        <p>Aqui debajo se listan los menues</p>

        <!-- Mensajes de acciones de opciones -->
        <?php echo my_msj_type($ControlMensajeError,$typeAlert);?>
        
        <!-- Opciones -->  
       
        <div class="row clearfix">
            <div class="col-xs-12 column">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="btn btn-default btn-xs navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                              <span class="glyphicon glyphicon-search"></span>
                            </button>
                            <?php echo anchor('menues/agregar','Crear Menu',array('class'=>'btn btn-primary navbar-btn marge-izq-btn-nav'));?>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

                            <?php echo form_open(null,array('class' => 'navbar-form navbar-right','role'=>'search', 'id'=>'form_search_table', 'style'=>"border:0px"))?>    
                                <?php $options = array('name'  => 'Nombre','controlador'=> 'Controlador','accion'=> 'Accion','url' => 'Url');?>
                                    <div class="form-group">
                                        <?php echo form_dropdown('tBus',$options,'name','class=form-control');?>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <?php echo form_input(array('type'=>'text','class'=>'form-control','name'=>'datoBus','placeholder'=>'Buscar...'))?>
                                            <div class="input-group-btn">
                                                <?php echo form_button(array('type'=>'submit','content'=>'<span class="glyphicon glyphicon-search"></span>','class'=>'btn btn-default'))?>
                                            </div>
                                        </div>
                                    </div>
                            <?php echo form_close();?>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        
        
        <!-- fin Opciones -->  
             
        
        <div class='space-white'></div>
        <!-- Grilla -->
        <table class='footable'>
            <thead>
                <tr>
                    <th data-sort-initial='true' data-sort-ignore='true'>Id</th>
                    <th data-sort-ignore='true'>Nombre</th>
                    <th data-hide='phone,tablet' data-sort-ignore='true'>Controlador</th>
                    <th data-hide='phone,tablet' data-sort-ignore='true'>Accion</th>
                    <th data-hide='phone,tablet' data-sort-ignore='true'>Url</th>
                    <th data-hide='phone,tablet' data-sort-ignore='true'>Orden</th>
                    <th data-hide='phone,tablet' data-sort-ignore='true'>Acceso Libre</th>                    
                    <th data-hide='phone,tablet' data-sort-ignore='true'>Visible en barra</th>
                    <th data-hide='phone,tablet' data-sort-ignore='true'>Solo con login/Menu Ppal</th>
                    <th data-hide='phone' data-sort-ignore='true'>Creado</th>
                    <th data-hide='phone' data-sort-ignore='true'>Actualizado</th>
                    <th data-hide='phone' data-sort-ignore='true'>Acciones</th>                
                </tr>
            </thead>
            <tbody>
               <?php foreach ($menues as $registro) {?>                   
                    <tr>
                        <td><?php echo $registro->id_menu ?></td>
                        <td><?php echo $registro->name ?></td>
                        <td><?php echo $registro->controlador ?></td>
                        <td><?php echo $registro->accion ?></td>
                        <td><?php echo $registro->url ?></td>
                        <td><?php echo $registro->orden ?></td>
                        <td><?php echo $registro->libre ?></td>
                        <td><?php echo $registro->visible ?></td>
                        <td><?php echo $registro->showlogin ?></td>
                        <td><?php echo date('d/m/Y',  strtotime($registro->created)) ?></td>
                        <td><?php echo date('d/m/Y',  strtotime($registro->updated)) ?></td>
                        <td>
                            <?php echo anchor('menues/editar/'.$registro->id_menu,'<span class="glyphicon glyphicon-pencil"></span>',array('title'=>'Editar','class'=>'btn btn-default'))?>
                            <?php echo anchor('menues/editarImg/'.$registro->id_menu,'<span class="glyphicon glyphicon-picture"></span>',array('title'=>'Editar Imagen','class'=>'btn btn-default'))?>
                            <?php echo anchor('menues/deshabilitar/'.$registro->id_menu,'<span class="glyphicon glyphicon-remove-circle"></span>',array('title'=>'Deshavilitar','class'=>'btn btn-default'))?>
                        </td>
                    </tr>
                <?php }?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="12">
                        <?php echo $pagination;?>
                    </td>
                </tr>
            </tfoot>
        </table>
        <div class='space-white'></div>

        <!-- Opciones end grilla --> 
        <div class='btn-group'><?php echo anchor('menues/agregar','Crear Menu',array('class'=>'btn btn-primary'));?></div>
        
    </div>
</div>