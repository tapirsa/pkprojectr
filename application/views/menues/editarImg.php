
<div class="panel panel-default">
  <div class="panel-heading"><h4 class="text-info">Editar Imagen Menu</h4></div>
  <div class="panel-body">
    <!-- Algun Comentario adicional -->
    <p>Por favor ingrese la Imagen del Menu.</p>

    <!-- Formulario -->
    <div class="row clearfix">
      <div class=" col-sm-2  column"> </div>
        <div class="col-xs-12 col-sm-8 column">
             <div class="well well-small">
                <?php echo my_msj_type($ControlMensajeError,isset($typeAlert)?$typeAlert:1);?>
                 
                <?php echo form_open_multipart(null,array('class'=>'form-horizontal'));?>
                  <div class="form-group">
                      <?php echo form_label('Id','id_menu', array('class'=>'col-sm-3 control-label'));?>                       
                      <div class="col-sm-8">
                         <p class="form-control-static"><?php echo $this->data['registro']->id_menu;?></p>
                         <?php echo form_hidden('id_menu',$this->data['registro']->id_menu);?>
                      </div>
                    </div>

                    <div class="form-group">
                      <?php echo form_label('Menu','name', array('class'=>'col-sm-3 control-label'));?> 
                      <div class="col-sm-8">
                        <p class="form-control-static"><?php echo $this->data['registro']->name;?></p>
                          <?php echo form_hidden('name',$this->data['registro']->name);?>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <?php echo form_label('Creado','created', array('class'=>'col-sm-3 control-label'));?> 
                      <div class="col-sm-8">
                        <p class="form-control-static"><?php echo $registro->created;?></p>
                          <?php echo form_hidden('created',$this->data['registro']->created);?>
                      </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Actualizado','updated', array('class'=>'col-sm-3 control-label'));?> 
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $registro->updated;?></p>
                            <?php echo form_hidden('updated',$this->data['registro']->updated);?>
                        </div>
                    </div>

                    <div class="form-group">
                    	<?php echo form_label('Imagen Actual','imgVieja', array('class'=>'col-sm-3 control-label'));?>
                    	<div class="col-sm-8">
                    		<?php $urlImg=base_url('img/imgOpciones/'.$this->data['registro']->imgOp);?>
                    		<img id="imgVieja" src="<?php echo $urlImg ?>" class="img-responsive"></img>
                    	</div>
                	</div>

                    <div class="form-group">
                    	<?php echo form_label('Nueva Imagen','imputImg',array('class'=>'col-sm-3 control-label'));?>
                    	<div class="col-sm-8">
                        	<?php echo form_input(array('type'=>'file','name'=>'imputImg','id'=>'imputImg'));?>
                    	</div>                   
                	</div>                   
                    
                    
                    <div class="form-group">
                      <div class="col-md-offset-3 col-md-8 btn-group">
                          <?php echo form_button(array('type'=>'submit','content'=>'Modificar','class'=>'btn btn-primary'));?>
                          <?php echo anchor('menues/index','Cancelar',array('class'=>'btn btn-default'));?>
                      </div>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
        <div class=" col-sm-2  column"></div>
    </div>
  </div>
</div>    