<div class="panel panel-default">
    <div class="panel-heading"><h4 class="text-info">Crear Menu</h4></div>

    <div class="panel-body">
        <!-- Algun Comentario adicional -->
        <p>Por favor ingrese la informacion del Menu.</p>

        <!-- Formulario -->
        <div class="row clearfix">
            <div class=" col-sm-2  column"> </div>
            <div class="col-xs-12 col-sm-8 column">
                <div class="well well-small">

                    <?php echo my_msj_type($ControlMensajeError,$typeAlert);?>

                    <?php echo  form_open(null,array('class'=>'form-horizontal','id'=>'form_create_menu'));?>

                    <div class="form-group">
                        <?php echo form_label('Nombre','name', array('class'=>'col-sm-3 control-label'));?>
                        <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'name','id'=>'name','placeholder'=>'Nombre...', 'value'=>set_value('name')));?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Controlador','controlador', array('class'=>'col-sm-3 control-label'));?>
                        <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'controlador','id'=>'controlador','placeholder'=>'Controlador...','value'=>set_value('controlador')));?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Accion','accion', array('class'=>'col-sm-3 control-label'));?>
                        <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'accion','id'=>'accion','placeholder'=>'Accion...','value'=>set_value('accion')));?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Url','url', array('class'=>'col-sm-3 control-label'));?>
                        <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'url','id'=>'url','placeholder'=>'Url...','value'=>set_value('url')));?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Orden','orden', array('class'=>'col-sm-3 control-label'));?>
                        <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'orden','id'=>'orden','placeholder'=>'Orden...','value'=>set_value('orden')));?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Acceso Libre','libre',array('class'=>'col-sm-3 control-label'));?>
                        <div class="col-sm-8">
                            <?php echo form_dropdown('libre',array('0'=>'NO','1'=>'SI'),0,'class="form-control" onChange="habilitar($(this).val())"');?>
                        </div>
                    </div>
                    
                    <div class="form-group" id="idVisible" style="display:none;">
                        <?php echo form_label('Visible en barra','visible',array('class'=>'col-sm-3 control-label'));?>
                        <div class="col-sm-8">
                            <?php echo form_dropdown('visible',array('0'=>'NO','1'=>'SI'),1,'class=form-control');?>
                        </div>
                    </div>

                    <div class="form-group" id="idSL">
                        <div id="idLibre" style="display:none;"><?php echo form_label('Solo con login','showlogin',array('class'=>'col-sm-3 control-label'));?></div>
                        <div id="idAdmin"><?php echo form_label('Menu Ppal','showlogin',array('class'=>'col-sm-3 control-label'));?></div>
                        <div class="col-sm-8">
                            <?php echo form_dropdown('showlogin',array('0'=>'NO','1'=>'SI','2'=>'Siempre'),0,'class=form-control');?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php echo form_label('Descripcion','decripcionOp',array('class'=>'col-sm-3 control-label'));?>
                        <div class="col-sm-8">
                            <?php echo form_textarea(array('id'=>'decripcionOp','name'=>'decripcionOp')) ?>
                        </div>
                    </div>                     

                    <div class="form-group">
                        <div class='col-md-offset-3 col-md-8 btn-group'>
                            <?php echo form_button(array('type'=>'submit','content'=>'Crear','class'=>'btn btn-primary'));?>
                            <?php echo anchor('menues/index','Cancelar',array('class'=>'btn btn-default'));?>
                        </div>
                    </div>

                    <?php echo form_close();?>
                </div>
            </div>
            <div class=" col-sm-2  column"></div>
        </div>

    </div>
</div>