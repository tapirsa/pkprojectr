<div class="panel panel-default">
    <div class="panel-heading"><h4 class="text-info">Editar Menu</h4></div>

    <div class="panel-body">
        <!-- Algun Comentario adicional -->
        <p>Por favor ingrese la informacion del Menu.</p>

        <!-- Formulario -->
        <div class="row clearfix">
            <div class=" col-sm-2  column"></div>
            <div class="col-xs-12 col-sm-8 column">
                 <div class="well well-small">
                    <?php echo my_msj_type($ControlMensajeError,isset($typeAlert)?$typeAlert:1);?>

                    <?php echo form_open(null,array('class'=>'form-horizontal','id'=>'form_create_menu'));?>

                        <div class="form-group">
                            <?php echo form_label('Id','id_menu', array('class'=>'col-sm-3 control-label'));?> 
                            <div class="col-sm-8">
                                <p class="form-control-static"><?php echo $registro->id_menu;?></p>
                            </div>
                            <?php echo form_hidden('id_menu',$registro->id_menu);?>
                        </div>

                        <div class="form-group">
                          <?php echo form_label('Nombre','name', array('class'=>'col-sm-3 control-label'));?>
                          <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'name','id'=>'name','placeholder'=>'Nombre...', 'value'=>set_value('name',$registro->name)));?>
                          </div>
                        </div>

                        <div class="form-group">
                          <?php echo form_label('Controlador','controlador', array('class'=>'col-sm-3 control-label'));?>
                          <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'controlador','id'=>'controlador','placeholder'=>'Controlador...','value'=>set_value('controlador',$registro->controlador)));?>
                          </div>
                        </div>

                        <div class="form-group">
                          <?php echo form_label('Accion','accion', array('class'=>'col-sm-3 control-label'));?>
                          <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'accion','id'=>'accion','placeholder'=>'Accion...','value'=>set_value('accion',$registro->accion)));?>
                          </div>
                        </div>

                        <div class="form-group">
                          <?php echo form_label('Url','url', array('class'=>'col-sm-3 control-label'));?>
                          <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'url','id'=>'url','placeholder'=>'Url...','value'=>set_value('url',$registro->url)));?>
                          </div>
                        </div>

                        <div class="form-group">
                          <?php echo form_label('Orden','orden', array('class'=>'col-sm-3 control-label'));?>
                          <div class="col-sm-8">
                            <?php echo form_input(array('type'=>'text','class' => 'form-control','name'=>'orden','id'=>'orden','placeholder'=>'Orden...','value'=>set_value('orden',$registro->orden)));?>
                          </div>
                        </div>

                        <div class="form-group">
                          <?php echo form_label('Acceso Libre','libre',array('class'=>'col-sm-3 control-label'));?>
                          <div class="col-sm-3">
                           <?php echo form_dropdown('libre',array('0'=>'NO','1'=>'SI'),$registro->libre,'class="form-control" onChange="habilitar($(this).val())"');?>
                          </div>
                        </div>

                        <div class="form-group"  id="idVisible" style="display:none;">
                            <?php echo form_label('Visible en barra','visible',array('class'=>'col-sm-3 control-label'));?>
                            <div class="col-sm-3">
                                <?php echo form_dropdown('visible',array('0'=>'NO','1'=>'SI'),$registro->visible,"class='form-control'");?>
                            </div>
                        </div>

                        <div class="form-group" id="idSL">
                            <div id="idLibre" style="display:none;"><?php echo form_label('Solo con login','showlogin',array('class'=>'col-sm-3 control-label'));?></div>
                            <div id="idAdmin"><?php echo form_label('Menu Ppal','showlogin',array('class'=>'col-sm-3 control-label'));?></div>                            
                            <div class="col-sm-3">
                                <?php echo form_dropdown('showlogin',array('0'=>'NO','1'=>'SI','2'=>'Siempre'),$registro->showlogin,"class='form-control'");?>
                            </div>
                        </div>

                        <div class="form-group">
                          <?php echo form_label('Creado','created', array('class'=>'col-sm-3 control-label'));?> 
                          <div class="col-sm-8">
                              <p class="form-control-static"><?php echo date('d/m/Y',strtotime($registro->created));?></p>
                              <?php echo form_hidden('created',$registro->created);?>
                          </div>
                        </div>

                        <div class="form-group">
                            <?php echo form_label('Actualizado','updated', array('class'=>'col-sm-3 control-label'));?> 
                            <div class="col-sm-8">
                                <p class="form-control-static"><?php echo date('d/m/Y',strtotime($registro->updated));?></p>
                                <?php echo form_hidden('updated',$registro->updated);?>
                            </div>
                        </div>

                        <div class="form-group">
                        <?php echo form_label('Descripcion','decripcionOp',array('class'=>'col-sm-3 control-label'));?>
                          <div class="col-sm-8">
                            <?php echo form_textarea(array('id'=>'decripcionOp','name'=>'decripcionOp','value'=>set_value('decripcionOp',$registro->decripcionOp))) ?>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-md-offset-3 col-md-8 btn-group">
                              <?php echo form_button(array('type'=>'submit','content'=>'Modificar','class'=>'btn btn-primary'));?>
                              <?php echo anchor('menues/index','Cancelar',array('class'=>'btn btn-default'));?>
                          </div>
                        </div>
                    <?php echo form_close();?>
                </div>
            </div>
            <div class=" col-sm-2  column"></div>
        </div>
    </div>
</div>    
<script type="text/javascript">cargarCapasOpMenu(<?php echo $registro->libre?>)</script>