<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->layout->setLayout('home_tpl');
        $this->layout->setKeywords('soluciones informaticas desarrollo Software app servidores hosting medida');
        $this->layout->setDescripcion('Desarrollo a medida de sistemas web');
        $this->load->helper('captcha');
    }
    
    public function index(){
        $this->layout->setLayout('home_Aux_tpl');

        $js = array(base_url('js/commun.js'));
        //llamamos a una librería js

        $this->layout->js($js);

        $this->lang->load('home');

        $this->data['titulo'] ="Inicio";

        $registro['var1'] = "hola mundo tst";

        $this->data['noticiasView'] = $this->load->view('home/index/noticias.php', $registro, true);

        $registro['var2'] = "se gargo la vista de galeria de imagenes";
        
        $this->data['galeriaImagenesView'] = $this->load->view('home/index/galeria_imagenes.php', $registro, true);

        $this->layout->view('index',$this->data);
    }

    public function denegar(){
        $this->layout->setLayout('base_tpl');
        $urlBack = 'home/principal';
        if(isset($_SERVER['HTTP_REFERER'])){
            $urlBack = $_SERVER['HTTP_REFERER'];
        }
        $this->data['urlBack'] = $urlBack;

        $this->data['titulo'] ="Acceso Denegado";
        $this->data['ControlMensajeError'] = "Acceso Denegado. Usted no posee los privilegios necesasrios.comuniquese con el administrados del sitio. Muchas gracios"; 
        $this->data['typeAlert'] = 3;        
        $this->layout->view('denegar',$this->data);
    }
    
    public function contactenos(){
        $this->load->model('contactos_model');
        $this->lang->load('contactos');
        $this->load->library('email');
        $this->data['titulo'] ="Contáctenos";
        $this->data['contacto'] = array('first_name'=>'','last_name'=>'','email'=>'','phone'=>'','subject'=>'','mensaje'=>'');
 
        if($this->input->post()){

            if(strtolower($this->input->post('captcha')) == strtolower($this->session->userdata('captcha'))){
                //validate form input
                $this->form_validation->set_rules('first_name', $this->lang->line('add_contacto_validation_nombre_label'), 'required|xss_clean');
                $this->form_validation->set_rules('last_name', $this->lang->line('add_contacto_validation_apellido_label'), 'xss_clean');
                $this->form_validation->set_rules('email', $this->lang->line('add_contacto_validation_email_label'), 'required|valid_email');
                $this->form_validation->set_rules('subject', $this->lang->line('add_contacto_validation_subject_label'), 'required');
                $this->form_validation->set_rules('mensaje', $this->lang->line('add_contacto_validation_mensaje_label'), 'required');

                if ($this->form_validation->run() == true )
                {
                    $registro = $this->input->post();
                    $registro['created'] = date('Y/m/d H:i');
                    $registro['updated'] = date('Y/m/d H:i');
                    $registro['estado'] = 1;
                    $registro['was_answered'] = 3;
                    unset($registro['captcha']);

                    if($this->contactos_model->insert($registro)){
                        $this->session->set_flashdata(array('message'=>'Se registro su consulta de forma correcta. Gracias!','typeAlert'=>1));

                        $message = $this->load->view('home/email/contacto.tpl.php', $registro, true);

                        $this->load->helper('config_smtp');
                        $this->email->initialize(get_config_smtp());

                        $this->email->clear();
                        $this->email->from('info@tapir.com.ar');
                        $this->email->to('raullazarofrias@gmail.com,claudio0185@gmail.com,buitrehga@gmail.com');
                        $this->email->subject('Se registro un Contacto');
                        $this->email->message($message);

                        $this->email->send();

                    } else {
                        $this->session->set_flashdata(array('message'=>'NO se registro su consulta de forma correcta por favor intente mas tarde. Gracias!','typeAlert'=>3));
                    }
                    redirect('home/contactenos'); 
                }
            } else {
                $this->session->set_flashdata(array('contacto'=> $this->input->post(),
                                                    'message'=>'Caracteres de la imagen incorrecto',
                                                    'typeAlert'=>3));
                redirect('home/contactenos'); 

            }
        }

        $this->data['captcha'] = $this->GetCaptcha();
        

        //the user is not logging in so display the login page
        //set the flash data error message if there is one
        if(validation_errors()){
            $this->data['message'] = validation_errors();
            $this->data['typeAlert'] = 3;
        } else {
            $this->data['message'] = $this->session->flashdata('message');
            $this->data['typeAlert'] = $this->session->flashdata('typeAlert');

            if($this->session->flashdata('contacto')){
                $this->data['contacto'] = $this->session->flashdata('contacto'); 
            }
        }


        $ccs =array(base_url('css/bootstrapvalidator/bootstrapValidator.min.css'));
        $js = array(base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                    base_url('js/bootstrapvalidator/validaciones.js'));

        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);

        $this->layout->view('contactenos',$this->data);
    }


    private function GetCaptcha()
    {
        //creamos un random alfanumerico de longitud 6 
        //para nuestro captcha y sesión captcha
        $this->rand = random_string('alnum', 4); 

        //creamos una sesión con el string del captcha que hemos creado
        //para utilizarlo en la función callback
        $this->session->set_userdata('captcha', $this->rand);

        //configuramos el captcha
        $conf_captcha = array(
            'word'   => $this->rand,
            'img_path' => './img/captcha/',
            'img_url' =>  base_url().'img/captcha/',
            //fuente utilizada por mi, poner la que tengáis
            'font_path' => '',
            'img_width' => '100',
            'img_height' => '50', 
            //decimos que pasados 10 minutos elimine todas las imágenes
            //que sobrepasen ese tiempo
            'expiration' => 600 
        );
 
        //guardamos la info del captcha en $cap
        $cap = create_captcha($conf_captcha);//,base_url().'img/captcha',base_url().'/img/captcha/','');

        //devolvemos el captcha para utilizarlo en la vista
        return $cap;
    }

    

    public function principal(){
        $this->data['titulo'] ="Principal";
        $this->layout->view('principal',$this->data);

    }
}