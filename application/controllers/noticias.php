<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends CI_Controller {

    public function __construct(){
            parent::__construct();
            $this->layout->setLayout('base_tpl');
            $this->load->model('noticias_model');
          
    }

    public function index($tBus="ninguno",$datoBus="nada"){
        //en el caso de que se este haciendo algun filtrado por medio del formulario de busqueda
        if($this->input->post()){           
            $infoBus = $this->input->post();
            $tBus=$infoBus["tBus"];
            $datoBus=$infoBus["datoBus"];      
        }       

        $cantReg = $this->noticias_model->getCantReg($tBus,$datoBus);//obtnego la cantidad de registro de la consulta
        $urlPag='noticias/index/'.$tBus."/".$datoBus;
        //configuracion de la paginacion (url=controlador/accion/tipoBusqueda/datoBusqueda,Cantidad reg totales consulta,Cantidad Reg a Mostrar,segmento a caputar de la url que indica el nro de pagina)
        $config_page = pagination($urlPag,$cantReg,10,5);
        $this->data['pagination'] =  $config_page['pagination'];//link de la paginacion

        $this->data['ControlMensajeError'] = $this->session->flashdata('ControlMensajeError'); 
        $this->data['typeAlert'] = $this->session->flashdata('typeAlert');

        $this->data['titulo'] ="Noticias";

        $ccs = array(   base_url('css/bootstrapvalidator/bootstrapValidator.min.css'),
                        base_url('css/footable/footable.core.min.css'),
                        base_url('css/footable/footable.standalone.min.css'));

        $js = array(    base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                        base_url('js/bootstrapvalidator/validaciones.js'),
                        base_url('js/footable/footable.js'),
                        base_url('js/footable/footable.sort.js'),                        
                        base_url('js/footable/inicializador.js'));

        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);

        $this->data['noticias']=$this->noticias_model->getAllPagination($config_page,$tBus,$datoBus);
        $this->layout->view('index',$this->data);
    }   

    public function agregar(){
        $this->data['titulo'] ="Crear Noticia";
        if($this->input->post()){
            $this->form_validation->set_rules('tituloN','Titulo','required|max_length[200]|xss_clean');
            $this->form_validation->set_rules('copeteN','Copete','required|xss_clean');
            $this->form_validation->set_rules('descN','Noticias','required|xss_clean');      
            if ($this->form_validation->run()){                
                //captura de campos
                $registro['tituloN']= $this->input->post("tituloN");
                $registro['copeteN']= $this->input->post("copeteN");
                $registro['descN']= $this->input->post("descN");
                $registro['portadaN']= $this->input->post("portadaN");
                $registro['created']=date('Y/m/d H:i');
                $registro['updated']=date('Y/m/d H:i');

                //Cargar imagen de Noticia
                $config=configArchivoImg('./img/imgN/');                
                $this->load->library('upload', $config);   
                if ($this->upload->do_upload('files')){                    
                    //Notica con imagen nueva
                    $dataImg=$this->upload->data();
                    $registro['imgN']=$dataImg['file_name'];
                    $this->noticias_model->insert($registro);                    
                    $this->session->set_flashdata(array('ControlMensajeError'=>'Se creo la noticia.','typeAlert'=>1));
                    redirect('noticias/index');
                }
                else{                   
                    //Noticia con imagen default              
                    $this->noticias_model->insert($registro);
                    $this->session->set_flashdata(array('ControlMensajeError'=>'Se creo la noticia con Imagen default','typeAlert'=>1));
                    redirect('noticias/index');
                }
            }
        }

        $this->data['ControlMensajeError'] = validation_errors();
        $this->data['typeAlert'] = 3;

        $ccs =array(base_url('css/bootstrapvalidator/bootstrapValidator.min.css'));

        $js = array(    base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                        base_url('js/bootstrapvalidator/validaciones.js'),
                        base_url('js/tinymce/tinymce.min.js'),
                        base_url('js/iniciarTiny.js'));

        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);

        $this->layout->view('agregar',$this->data);
    }

    
    
    public function editar($id_noticia){
        $this->data['titulo'] ="Editar Noticia";	
        $this->data['registro'] = $this->noticias_model->getFind($id_noticia);

        if($this->input->post()){
            $this->form_validation->set_rules('tituloN','Titulo','required|max_length[200]|xss_clean');
            $this->form_validation->set_rules('copeteN','Copete','required|xss_clean');
            $this->form_validation->set_rules('descN','Noticias','required|xss_clean');      
            if ($this->form_validation->run()){

                //captura de campos
                $registro['id_noticia']= $this->input->post("id_noticia");
                $registro['tituloN']= $this->input->post("tituloN");
                $registro['copeteN']= $this->input->post("copeteN");
                $registro['descN']= $this->input->post("descN");
                $registro['portadaN']= $this->input->post("portadaN");               
                $registro['updated']=date('Y/m/d H:i');               

                //Cargar imagen de Noticia
                $config=configArchivoImg('./img/imgN/');                
                $this->load->library('upload', $config);   
                if ($this->upload->do_upload('files')){                    
                    //Notica con imagen nueva
                    $dataImg=$this->upload->data();
                    $registro['imgN']=$dataImg['file_name'];                    
                    $this->noticias_model->update($registro);
                    $this->session->set_flashdata(array('ControlMensajeError'=>'Se modifico la noticia.','typeAlert'=>1));                    
                    redirect('noticias/index');
                }
                else{                   
                    //Noticia con imagen default                    
                    $this->noticias_model->update($registro);
                    $this->session->set_flashdata(array('ControlMensajeError'=>'Se modifico la noticia con Imagen Actual','typeAlert'=>1));
                    redirect('noticias/index');
                }
            }               
            
        }

        $this->data['ControlMensajeError'] = validation_errors();
        $this->data['typeAlert'] = 3;

        $ccs =array(base_url('css/bootstrapvalidator/bootstrapValidator.min.css'));

        $js = array(    base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                        base_url('js/bootstrapvalidator/validaciones.js'),
                        base_url('js/tinymce/tinymce.min.js'),                        
                        base_url('js/iniciarTiny.js'));

        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);

        $this->layout->view('editar',$this->data);
    }

    public function deshabilitar($id_noticia){
        $this->noticias_model->deleteLogico($id_noticia);
        $this->session->set_flashdata(array('ControlMensajeError'=>'La Noticia se ha deshabilitado','typeAlert'=>1));
        redirect('noticias/index');
	}
}