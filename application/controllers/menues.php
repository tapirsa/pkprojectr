<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menues extends CI_Controller {

    public function __construct(){
            parent::__construct();
            $this->layout->setLayout('base_tpl');
            $this->load->model('menues_model');
            $this->load->library('menu_lib');
    }

    public function index($tBus="ninguno",$datoBus="nada"){
        //en el caso de que se este haciendo algun filtrado por medio del formulario de busqueda
        if($this->input->post()){           
            $infoBus = $this->input->post();
            $tBus=$infoBus["tBus"];
            $datoBus=$infoBus["datoBus"];      
        }       

        $cantReg = $this->menues_model->getCantReg($tBus,$datoBus);//obtnego la cantidad de registro de la consulta
        $urlPag='menues/index/'.$tBus."/".$datoBus;
        //configuracion de la paginacion (url=controlador/accion/tipoBusqueda/datoBusqueda,Cantidad reg totales consulta,Cantidad Reg a Mostrar,segmento a caputar de la url que indica el nro de pagina)
        $config_page = pagination($urlPag,$cantReg,10,5);
        $this->data['pagination'] =  $config_page['pagination'];//link de la paginacion

        $this->data['ControlMensajeError'] = $this->session->flashdata('ControlMensajeError'); 
        $this->data['typeAlert'] = $this->session->flashdata('typeAlert');

        $this->data['titulo'] ="Menues";

        $ccs = array(   base_url('css/bootstrapvalidator/bootstrapValidator.min.css'),
                        base_url('css/footable/footable.core.min.css'),
                        base_url('css/footable/footable.standalone.min.css'));

        $js = array(    base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                        base_url('js/bootstrapvalidator/validaciones.js'),
                        base_url('js/footable/footable.js'),
                        base_url('js/footable/footable.sort.js'),                        
                        base_url('js/footable/inicializador.js'));

        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);

        $this->data['menues']=$this->menues_model->getAllPagination($config_page,$tBus,$datoBus);
        $this->layout->view('index',$this->data);
    }

    public function my_validation(){
		$registro=$this->input->post();
		return $this->menu_lib->my_validation($registro);
	}

    public function agregar(){
        $this->data['titulo'] ="Crear Menu";
        if($this->input->post()){
            $this->form_validation->set_rules('name','Nombre','required|max_length[20]|xss_clean');
            $this->form_validation->set_rules('controlador','Controlador','xss_clean|callback_my_validation');
            $this->form_validation->set_rules('accion','Accion','xss_clean');
            $this->form_validation->set_rules('url','Url','xss_clean');
            $this->form_validation->set_rules('orden','Orden','required|is_natural|xss_clean');

            if ($this->form_validation->run()){
                $registro = $this->input->post();
                $registro['created']=date('Y/m/d H:i');
                $registro['updated']=date('Y/m/d H:i');                   
                $this->menues_model->insert($registro);
                $this->session->set_flashdata(array('ControlMensajeError'=>'Se creo el nuevo menu','typeAlert'=>1));
                redirect('menues/index');				
            }
        }		

        $this->data['ControlMensajeError'] = validation_errors();
        $this->data['typeAlert'] = 3;

        $ccs =array(base_url('css/bootstrapvalidator/bootstrapValidator.min.css'));

        $js = array(    base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                        base_url('js/bootstrapvalidator/validaciones.js'),
                        base_url('js/tinymce/tinymce.min.js'),
                        base_url('js/manejoMenues.js'),
                        base_url('js/iniciarTiny.js'));

        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);

        $this->layout->view('agregar',$this->data);
    }

    public function editar($id_menu){
        $this->data['titulo'] ="Editar Menu";	
        $this->data['registro'] = $this->menues_model->getFind($id_menu);

        if($this->input->post()){
            $this->form_validation->set_rules('name','Nombre','required|max_length[20]|xss_clean');
            $this->form_validation->set_rules('controlador','Controlador','xss_clean|callback_my_validation');
            $this->form_validation->set_rules('accion','Accion','xss_clean');
            $this->form_validation->set_rules('url','Url','xss_clean');
            $this->form_validation->set_rules('orden','Orden','required|is_natural');
            if($this->form_validation->run()){
                $registro = $this->input->post();
                $registro['updated']=date('Y/m/d H:i');
                $this->menues_model->update($registro);
                $this->session->set_flashdata(array('ControlMensajeError'=>'El menu se edito de forma correcta','typeAlert'=>1));
                redirect('menues/index');
            }
        }

        $this->data['ControlMensajeError'] = validation_errors();
        $this->data['typeAlert'] = 3;

        $ccs =array(base_url('css/bootstrapvalidator/bootstrapValidator.min.css'));

        $js = array(    base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                        base_url('js/bootstrapvalidator/validaciones.js'),
                        base_url('js/tinymce/tinymce.min.js'),
                        base_url('js/manejoMenues.js'),
                        base_url('js/iniciarTiny.js'));


        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);

        $this->layout->view('editar',$this->data);
    }

    public function deshabilitar($id_menu){
            if($this->menues_model->accesosDeMenu($id_menu)){
                $this->session->set_flashdata(array('ControlMensajeError'=>'No es posible deshabilitar el Menu ya que posee accesos activos asociados','typeAlert'=>3));
            }
            else{
                $this->menues_model->deleteLogico($id_menu);
                $this->session->set_flashdata(array('ControlMensajeError'=>'El menu se ha deshabilitado','typeAlert'=>1));
            }
            redirect('menues/index');
	}
        
    public function editarImg($id_menu){
        $this->data['titulo'] ="Cambiar Imagen Menu";
        //carga imagen al directorio correspondiente
        $config=configArchivoImg('./img/imgOpciones/');
        $this->load->library('upload', $config);   
        $this->data['registro'] = $this->menues_model->getFind($id_menu);

        if($this->input->post()){
            if ($this->upload->do_upload('imputImg')){
                $registro = $this->input->post();
                $registro['updated']=date('Y/m/d H:i');
                $dataImg=$this->upload->data();
                $registro['imgOp']=$dataImg['file_name'];
                $sql = $this->menues_model->update($registro);
                $this->load->library('img');//cargo libreria que redimensiona
                $this->img->rimg($registro['imgOp'], array('width' => 253, 'height' => 148,'alt' => $registro['imgOp'], 'title' => $registro['imgOp']));
                $this->session->set_flashdata(array('ControlMensajeError'=>'La imagen se modifico','typeAlert'=>1));               
                redirect('menues/index');
            }
        }
                
        $this->data['ControlMensajeError'] = validation_errors();
        $this->data['ControlMensajeError'] = $this->data['ControlMensajeError'].$this->upload->display_errors();//concateno error de imagen
        $this->data['typeAlert'] = 3;
        
        $this->layout->view('editarImg',$this->data);
    }
}