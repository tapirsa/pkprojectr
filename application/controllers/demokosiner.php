<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Demokosiner extends CI_Controller {

	function __construct()
      {
            parent::__construct();
            $this->layout->setLayout('demokosiner_tpl');
      }
        
      function index()
      {
             $this->data['titulo'] = "Demo Kosiner";

            $this->layout->view('index',$this->data );
      }
}