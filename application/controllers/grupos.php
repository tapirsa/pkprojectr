<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Grupos extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('grupos_model');
        $this->layout->setLayout('base_tpl');
    }
    
    // esto es un comentario de pruba
    public function index($tBus="ninguno",$datoBus="nada"){
         //en el caso de que se este haciendo algun filtrado por medio del formulario de busqueda
        if($this->input->post()){           
            $infoBus = $this->input->post();
            $tBus=$infoBus["tBus"];
            $datoBus=$infoBus["datoBus"];      
        }       

        $cantReg = $this->grupos_model->getCantReg($tBus,$datoBus);//obtnego la cantidad de registro de la consulta
        $urlPag='grupos/index/'.$tBus."/".$datoBus;
        //configuracion de la paginacion (url=controlador/accion/tipoBusqueda/datoBusqueda,Cantidad reg totales consulta,Cantidad Reg a Mostrar,segmento a caputar de la url que indica el nro de pagina)
        $config_page = pagination($urlPag,$cantReg,10,5);
        $this->data['pagination'] =  $config_page['pagination'];//link de la paginacion
     
        $this->data['titulo'] ="Grupos";
            
        $this->data['ControlMensajeError'] = $this->session->flashdata('ControlMensajeError'); 
        $this->data['typeAlert'] = $this->session->flashdata('typeAlert');

        
        $ccs = array(   base_url('css/bootstrapvalidator/bootstrapValidator.min.css'),
                        base_url('css/footable/footable.core.min.css'),
                        base_url('css/footable/footable.standalone.min.css'));
                
        $js = array(    base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                        base_url('js/bootstrapvalidator/validaciones.js'),
                        base_url('js/footable/footable.js'),
                        base_url('js/footable/footable.sort.js'),
                        base_url('js/footable/inicializador.js'));

        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);
        
        $this->data['grupos']=$this->grupos_model->getAllPagination($config_page,$tBus,$datoBus);
        
        $this->layout->view('index',$this->data);
    }

    
    public function agregar(){
        $this->data['titulo'] ="Crear Grupo";
        
        if($this->input->post()){
            if($this->form_validation->run('grupos/agregar')){
                $registro=$this->input->post();
                $registro['created'] = date('Y/m/d H:i');
                $registro['updated'] = date('Y/m/d H:i');
                $this->grupos_model->insert($registro);
                $this->session->set_flashdata(array('ControlMensajeError'=>'Se creo el nuevo grupo','typeAlert'=>1));
                redirect('grupos/index');
            }
        }
        
        $this->data['ControlMensajeError'] =  validation_errors();
        $this->data['typeAlert'] = 3;
        
        $ccs =array(base_url('css/bootstrapvalidator/bootstrapValidator.min.css'));

        $js = array(    base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                        base_url('js/bootstrapvalidator/validaciones.js'));
        
        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);

        $this->layout->view('agregar',$this->data);
    }

    public function editar($id){
        $this->data['titulo']='Editar Grupo';
        $this->data['registro']=$this->grupos_model->getFind($id);
        if($this->input->post()){
            if($this->form_validation->run('grupos/editar')){
                $registro = $this->input->post();
                $registro['updated'] = date('Y/m/d H:i');
                $this->grupos_model->update($registro);
                $this->session->set_flashdata(array('ControlMensajeError'=>'El grupo se edito de forma correcta','typeAlert'=>1));
                redirect('grupos/index');
            }
        }
        
        $this->data['ControlMensajeError'] = validation_errors();
        $this->data['typeAlert'] = 3;
        
        $ccs =array(base_url('css/bootstrapvalidator/bootstrapValidator.min.css'));

        $js = array(    base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                        base_url('js/bootstrapvalidator/validaciones.js'));

        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);
        
        $this->layout->view('editar',$this->data);
    }

    public function deshabilitar($id){
        if($this->grupos_model->usuariosDeGrupo($id)){
             $this->session->set_flashdata(array('ControlMensajeError'=>'No es posible deshabilitar el grupo ya que posee usuarios activos asociados',
                                                'typeAlert'=>3));
        }
        else{
            if($this->grupos_model->menuesDeGrupos($id)){
                $this->session->set_flashdata(array('ControlMensajeError'=>'No es posible deshabilitar el grupo ya que posee accesos de menu activos asocioados',
                                                    'typeAlert'=>3));
            }
            else{
                if($this->grupos_model->deleteLogico($id)){
                    $this->session->set_flashdata(array('ControlMensajeError'=>'El grupo se ha deshabilitado',
                                                        'typeAlert'=>1));
                }
                else{
                    $this->session->set_flashdata(array('ControlMensajeError'=>'No fue posible deshabilitar el grupo. Intete nuevamente',
                                                        'typeAlert'=>3));
                }
            }
        }
        redirect('grupos/index');
    }
}