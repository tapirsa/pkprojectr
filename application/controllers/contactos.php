<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactos extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->layout->setLayout('base_tpl');
        $this->lang->load('contactos');
        $this->load->model('contactos_model');
        $this->load->model('respuestas_model');
        $this->layout->setLayout('base_tpl');
    }
    
    
    public function index($tBus="ninguno",$datoBus="nada"){
        $this->data['titulo'] ="Contactos";
        
        $this->data['ControlMensajeError'] = $this->session->flashdata('ControlMensajeError'); 
        $this->data['typeAlert'] = $this->session->flashdata('typeAlert');
        
        $ccs = array(   base_url('css/footable/footable.core.min.css'),
                        base_url('css/footable/footable.standalone.min.css'));
                
        $js = array(    base_url('js/footable/footable.js'),
                        base_url('js/footable/footable.paginate.js'),
                        base_url('js/footable/footable.sort.js'),
                        base_url('js/footable/footable.filter.js'),
                        base_url('js/footable/inicializador.js'));

        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);
        
        $this->data['contactos']=$this->contactos_model->getAll();
        
        $this->layout->view('index',$this->data);
    }

    
    public function responder($id_contacto = -1){
        $optionEmail = -1;
        $message_error ='';
        $tipoAlerta = 3;
        $this->data['titulo'] ="Responder Consulta";
        $this->data['message_error'] = '';
        $this->data['typeAlert'] = -1;

        if($this->input->post()){
            
            //validate form input
            $this->form_validation->set_rules('respuesta', 'Respuesta', 'required|xss_clean');
            
            if($this->form_validation->run()){
                $registro = $this->input->post();
                $emailContacto = $registro['email'];
                $optionEmail = $registro['optResp'];// $this->input->post('optResp') ;
                $id_respuesta = (is_numeric($registro['id_respuesta']) && $registro['id_respuesta'] > 0 )? $registro['id_respuesta'] : -1;  

                unset($registro['optResp']);
                unset($registro['email']);

                if( $id_respuesta == -1 ){
                    $registro['id_usuario'] = $this->ion_auth->get_user_id();
                    $registro['created'] = date('Y/m/d H:i');
                    $registro['estado'] = 1;
                    $isOkSave = $this->respuestas_model->insert($registro);
                } else {
                    $registro['updated'] = date('Y/m/d H:i');
                    $isOkSave = $this->respuestas_model->update($registro);
                }

                if($isOkSave){
                    $message_error .= 'Se registro la respuesta correctamente';
                    $queEstadoContacto = 1;

                    if($optionEmail == 2){ //opcion de guardar y enviar
                        $message = $this->load->view('contactos/email/respuesta.tpl.php', $registro, true);

                        $this->load->helper('config_smtp');
                        $this->email->initialize(get_config_smtp());

                        $this->email->clear();
                        $this->email->from('info@tapir.com.ar');
                        $this->email->to($emailContacto);
                        $this->email->subject('Respuesta a su consulta en Tapir');
                        $this->email->message($message);

                        if($this->email->send()){
                            $message_error .= '<br> Se envio el email con exitos.';
                            $queEstadoContacto = 2;
                        }
                        else
                            $message_error .= '<br> Fallo el envio de email.';
                    }

                    if($this->contactos_model->updateEstado($id_contacto, $queEstadoContacto))
                        $message_error .= '<br> Se actualizo el estado del contacto con exitos.';
                    else
                        $message_error .= '<br> No se pudo actualizar el estado del contacto.';

                    $tipoAlerta = 1;
                } else {
                    $message_error .= 'No se registro la respuesta';
                    $tipoAlerta = 3;
                }

                //redirect index the contactos
                $this->session->set_flashdata('ControlMensajeError', $message_error);
                $this->session->set_flashdata('typeAlert',$tipoAlerta);
                redirect("contactos/index", 'refresh');
            }
        }
        
        if(validation_errors()){
            $this->data['message_error'] = validation_errors();
            $this->data['typeAlert'] = 3;
        }
        
        $ccs =array(base_url('css/bootstrapvalidator/bootstrapValidator.min.css'));

        $js = array(    base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                        base_url('js/tinymce/tinymce.min.js'),
                        base_url('js/iniciarTiny.js'),
                        base_url('js/bootstrapvalidator/validaciones.js'));
        
        //llamamos a una librería js
        $this->layout->js($js);
        //llamamos a una librería css
        $this->layout->css($ccs);

        $oContactoWithAnswer = $this->contactos_model->getWithAnswer($id_contacto);

        $this->data['contacto'] = $oContactoWithAnswer;

        $this->layout->view('responder',$this->data);
    }


    public function deshabilitar($id_contacto){
        if($this->contactos_model->deleteLogico($id_contacto)){
            $this->session->set_flashdata(array('ControlMensajeError'=>'El contacto se deshactivo exitosamene','typeAlert'=>2));
        } else {

            return false;
            $this->session->set_flashdata(array('ControlMensajeError'=>'No es posible deshabilitar el Contacto','typeAlert'=>3));
        }
        redirect('contactos/index');
    }


    
}