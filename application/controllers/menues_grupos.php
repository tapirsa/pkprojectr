<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menues_grupos extends CI_Controller {

	function __construct(){
            parent::__construct();
            $this->layout->setLayout('base_tpl');
            $this->load->model('menues_grupos_model');
            $this->load->model('menues_model');
            $this->form_validation->set_message('existe_combinacion','EL acceso ya esta creado');
	}
        
   
	public function index($tBus="ninguno",$datoBus="nada"){
             //en el caso de que se este haciendo algun filtrado por medio del formulario de busqueda
            if($this->input->post()){           
                $infoBus = $this->input->post();
                $tBus=$infoBus["tBus"];
                $datoBus=$infoBus["datoBus"];      
            }       

            $cantReg = $this->menues_grupos_model->getCantReg($tBus,$datoBus);//obtnego la cantidad de registro de la consulta
            $urlPag='menues_grupos/index/'.$tBus."/".$datoBus;
            //configuracion de la paginacion (url=controlador/accion/tipoBusqueda/datoBusqueda,Cantidad reg totales consulta,Cantidad Reg a Mostrar,segmento a caputar de la url que indica el nro de pagina)
            $config_page = pagination($urlPag,$cantReg,10,5);
            $this->data['pagination'] =  $config_page['pagination'];//link de la paginacion

            $this->data['titulo'] ="Seguridad";        

            $ccs = array(   base_url('css/bootstrapvalidator/bootstrapValidator.min.css'),
                            base_url('css/footable/footable.core.min.css'),
                            base_url('css/footable/footable.standalone.min.css'));

            $js = array(base_url('js/bootstrapvalidator/bootstrapValidator.min.js'),
                        base_url('js/bootstrapvalidator/validaciones.js'),
                        base_url('js/footable/footable.js'),
                        base_url('js/footable/footable.sort.js'),
                        base_url('js/footable/inicializador.js'));

            $this->data['ControlMensajeError'] = $this->session->flashdata('ControlMensajeError');
            $this->data['typeAlert'] = $this->session->flashdata('typeAlert');

            //llamamos a una librería js
            $this->layout->js($js);
            //llamamos a una librería css
            $this->layout->css($ccs);

            $this->data['menues_grupos']=$this->menues_grupos_model->getAllPagination($config_page,$tBus,$datoBus);

            $this->layout->view('index',$this->data);
	}

        public function existe_combinacion(){
            $registro = $this->input->post();
            return $this->menues_grupos_model->existeCombinacion($registro);
        }

        public function agregar(){
            $this->data['titulo']='Agregar vinculo';
            if($this->input->post()){                        
                $this->form_validation->set_rules('id_menu','id menu','callback_existe_combinacion');
                if($this->form_validation->run()){
                    $registro = $this->input->post();
                    $registro['created'] = date('Y/m/d H:i');
                    $registro['updated'] = date('Y/m/d H:i');
                    $this->menues_grupos_model->insert($registro);
                    $this->session->set_flashdata(array('ControlMensajeError'=>'Se creo el acceso de forma correcta','typeAlert'=>1));
                    redirect('menues_grupos/index');
                }
            }

            $this->data['ControlMensajeError'] = validation_errors();
            $this->data['typeAlert'] = 3;

            //obtener opciones de formulario
            $this->data['opciones_menu'] = $this->menues_grupos_model->opcionesMenu();
            $this->data['opciones_grupo'] = $this->menues_grupos_model->opcionesGrupo();                

            $this->layout->view('agregar',$this->data);
        }

        public function editar($id_menu_group){
            $this->data['titulo'] = 'Editar Acceso';
            if($this->input->post()){
                    $this->form_validation->set_rules('id_menu_group','id menu grupo','callback_existe_combinacion');
                    if($this->form_validation->run()){
                            $registro = $this->input->post();
                            $registro['updated']=date('Y/m/d H:i');
                            $this->menues_grupos_model->update($registro);
                            $this->session->set_flashdata(array('ControlMensajeError'=>'El acceso se modifico.','typeAlert'=>1));
                            redirect('menues_grupos/index');
                    }
            }
            
            $this->data['ControlMensajeError'] = validation_errors();
            $this->data['typeAlert'] = 3;

             //obtener opciones de formulario
            $this->data['opciones_menu'] = $this->menues_grupos_model->opcionesMenu();
            $this->data['opciones_grupo'] = $this->menues_grupos_model->opcionesGrupo();

            //obtener valores actuales del formulario
            $this->data['menues_grupos'] = $this->menues_grupos_model->getFind($id_menu_group);

            $this->layout->view('editar',$this->data);

        }

        public function deshabilitar($id_menu_group){
            if($this->menues_grupos_model->delete($id_menu_group)){
                $this->session->set_flashdata(array('ControlMensajeError'=>'El acceso se ha deshabilitado','typeAlert'=>1));        
            }
            else{
                $this->session->set_flashdata(array('ControlMensajeError'=>'El acceso no se ha deshabilitado.Intente nuevemente','typeAlert'=>3));
            }
            redirect('menues_grupos/index');
        }
}