-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-01-2016 a las 21:50:09
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `pkprojectdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE IF NOT EXISTS `contactos` (
  `id_contacto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `subject` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `mensaje` text COLLATE utf8_spanish2_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `was_answered` tinyint(3) NOT NULL DEFAULT '3',
  PRIMARY KEY (`id_contacto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `created`, `updated`, `estado`) VALUES
(1, 'admin', 'estets', NULL, '2014-09-27 17:17:00', 1),
(2, 'members', 'Usuarios comunes', '2015-04-18 22:59:00', '2015-04-18 23:06:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menues`
--

CREATE TABLE IF NOT EXISTS `menues` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `controlador` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `accion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `imgOp` varchar(100) NOT NULL DEFAULT 'default.png',
  `libre` tinyint(1) NOT NULL DEFAULT '0',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `showlogin` tinyint(3) NOT NULL DEFAULT '2',
  `decripcionOp` varchar(255) NOT NULL,
  `iconfont` varchar(80) NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Volcado de datos para la tabla `menues`
--

INSERT INTO `menues` (`id_menu`, `name`, `created`, `updated`, `controlador`, `accion`, `url`, `orden`, `imgOp`, `libre`, `estado`, `visible`, `showlogin`, `decripcionOp`, `iconfont`) VALUES
(1, 'Inicio', '2015-04-01 00:00:00', '2015-04-01 00:00:00', 'home', 'index', '', 1, 'default.png', 1, 1, 1, 2, '', 'glyphicon glyphicon-home'),
(2, 'Ingresar', '2015-04-01 00:00:00', '2015-04-19 00:43:00', 'auth', 'login', '', 2, 'default.png', 1, 1, 0, 0, '<p>s</p>', ''),
(3, 'Principal', '2015-04-01 00:00:00', '2015-04-01 00:00:00', 'home', 'principal', '', 3, 'default.png', 1, 1, 1, 1, '', 'glyphicon glyphicon-th'),
(4, 'Salir', '2015-04-01 00:00:00', '2015-04-20 21:28:00', 'auth', 'logout', '', 100, 'default.png', 1, 1, 1, 1, '<p>-</p>', 'glyphicon glyphicon-off'),
(5, 'Usuarios', '2015-04-01 00:00:00', '2015-04-01 00:00:00', 'auth', 'index', '', 5, 'usuario.jpg', 0, 1, 0, 1, '', ''),
(6, 'Grupos', '2015-04-02 00:00:00', '2016-01-16 16:46:00', 'grupos', 'index', '', 6, 'default.png', 0, 1, 0, 1, 'Administración de Grutos', ''),
(7, 'Menues', '2015-04-01 00:00:00', '2015-04-01 00:00:00', 'menues', 'index', '', 7, 'default.png', 0, 1, 0, 1, '', ''),
(8, 'Seguridad', '2015-04-01 00:00:00', '2015-04-01 00:00:00', 'menues_grupos', 'index', '', 8, 'default.png', 0, 1, 0, 1, '', ''),
(9, 'Contactenos', '2015-04-01 00:00:00', '2015-04-20 22:09:00', 'home', 'contactenos', '', 4, 'default.png', 1, 1, 1, 2, '<p>-</p>', 'glyphicon glyphicon-envelope'),
(10, 'Admi Contactos', '2015-04-01 00:00:00', '2015-04-01 00:00:00', 'contactos', 'index', '', 10, 'contactos.jpg', 0, 1, 1, 2, '', ''),
(11, 'Olvide contraseña', '2015-04-01 00:00:00', '2015-10-03 18:13:00', 'auth', 'forgot_password', '', 11, 'default.png', 1, 1, 0, 0, '', ''),
(12, 'Activar Usuario', '2015-04-01 00:00:00', '2015-10-03 11:22:00', 'auth', 'activate', '', 12, 'default.png', 0, 1, 0, 0, '', ''),
(13, 'Reset Password', '2015-04-25 20:30:00', '2015-04-25 20:30:00', 'auth', 'reset_password', '', 13, 'default.png', 1, 1, 0, 0, '<p>-</p>', ''),
(14, 'Privilegios', '2015-04-27 18:25:00', '2015-04-27 18:25:00', 'home', 'denegar', '', 14, 'default.png', 1, 1, 0, 0, '<p>a</p>', ''),
(15, 'Crear Menu', '2015-07-01 00:00:00', '2015-10-03 17:52:00', 'menues', 'agregar', '', 15, 'default.png', 0, 1, 1, 0, '', ''),
(16, 'Editar Menu', '2015-07-01 00:00:00', '2015-10-03 10:19:00', 'menues', 'editar', '', 16, 'default.png', 0, 1, 0, 0, '', ''),
(17, 'Editar IMG Menu', '2015-07-01 00:00:00', '2015-10-03 10:15:00', 'menues', 'editarImg', '', 17, 'default.png', 0, 1, 1, 0, '', ''),
(18, 'Deshabilitar Menu', '2015-07-01 00:00:00', '2015-10-03 10:15:00', 'menues', 'deshabilitar', '', 18, 'default.png', 0, 1, 1, 0, '', ''),
(19, 'Crear Acceso Menu', '2015-07-01 00:00:00', '2015-10-03 10:19:00', 'menues_grupos', 'agregar', '', 19, 'default.png', 0, 1, 0, 0, '', ''),
(20, 'Editar Acceso Menu', '2015-07-01 00:00:00', '2015-10-03 10:19:00', 'menues_grupos', 'editar', '', 20, 'default.png', 0, 1, 0, 0, '', ''),
(21, 'Deshabilitar Acceso', '2015-07-01 00:00:00', '2015-10-03 10:21:00', 'menues_grupos', 'deshabilitar', '', 21, 'default.png', 0, 1, 0, 0, '', ''),
(22, 'Crear Grupos', '2015-07-01 00:00:00', '2015-10-03 10:17:00', 'grupos', 'agregar', '', 22, 'default.png', 0, 1, 0, 0, '', ''),
(23, 'Editar Grupos', '2015-07-01 00:00:00', '2015-10-03 10:17:00', 'grupos', 'editar', '', 23, 'default.png', 0, 1, 0, 0, '', ''),
(24, 'Deshabilitar Grupos', '2015-07-01 00:00:00', '2015-10-03 10:17:00', 'grupos', 'deshabilitar', '', 24, 'default.png', 0, 1, 0, 0, '', ''),
(25, 'Crear Usuario', '2015-07-01 00:00:00', '2015-10-03 10:17:00', 'auth', 'create_user', '', 25, 'default.png', 0, 1, 0, 0, '', ''),
(26, 'Editar Usuario', '2015-07-01 00:00:00', '2015-10-03 10:18:00', 'auth', 'edit_user', '', 26, 'default.png', 0, 1, 0, 0, '', ''),
(27, 'Desactivar Usuario', '2015-07-01 00:00:00', '2015-10-03 11:22:00', 'auth', 'deactivate', '', 27, 'default.png', 0, 1, 0, 0, '', ''),
(28, 'Responder Contacto', '2015-07-01 00:00:00', '2015-10-03 10:19:00', 'contactos', 'responder', '', 28, 'default.png', 0, 1, 0, 0, '', ''),
(29, 'Deshabilitar Contact', '2015-07-01 00:00:00', '2015-10-03 10:24:00', 'contactos', 'deshabilitar', '', 29, 'default.png', 0, 1, 0, 0, '', ''),
(30, 'Demo Kosiner', '2016-01-09 19:21:00', '2016-01-09 19:22:00', 'demokosiner', 'index', '', 5, 'default.png', 1, 1, 0, 0, '', ''),
(31, 'Noticias', '2016-01-30 21:40:00', '2016-01-30 21:41:00', 'noticias', 'index', '', 34, 'default.png', 0, 1, 1, 1, '', ''),
(32, 'Crear Noticia', '2016-01-30 21:41:00', '2016-01-30 21:41:00', 'noticias', 'agregar', '', 35, 'default.png', 0, 1, 1, 0, '', ''),
(33, 'Editar Noticia', '2016-01-30 21:42:00', '2016-01-30 21:42:00', 'noticias', 'editar', '', 36, 'default.png', 0, 1, 1, 0, '', ''),
(34, 'Deshabilitar Noticia', '2016-01-30 21:42:00', '2016-01-30 21:42:00', 'noticias', 'deshabilitar', '', 37, 'default.png', 0, 1, 1, 0, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menues-groups`
--

CREATE TABLE IF NOT EXISTS `menues-groups` (
  `id_menu_group` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `id_menu` int(11) NOT NULL,
  `id_group` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id_menu_group`),
  KEY `id_menu` (`id_menu`),
  KEY `id_group` (`id_group`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Volcado de datos para la tabla `menues-groups`
--

INSERT INTO `menues-groups` (`id_menu_group`, `created`, `updated`, `id_menu`, `id_group`) VALUES
(1, '2015-04-01 00:00:00', '2015-04-01 00:00:00', 3, 1),
(2, '2015-04-01 00:00:00', '2015-04-01 00:00:00', 5, 1),
(3, '2015-04-01 00:00:00', '2015-04-01 00:00:00', 6, 1),
(5, '2015-04-01 00:00:00', '2015-04-01 00:00:00', 8, 1),
(12, '2015-07-01 00:00:00', '2015-07-01 00:00:00', 20, 1),
(13, '2015-07-01 00:00:00', '2015-07-01 00:00:00', 21, 1),
(21, '2015-07-01 00:00:00', '2015-07-01 00:00:00', 28, 1),
(22, '2015-07-01 00:00:00', '2015-07-01 00:00:00', 29, 1),
(24, '2016-01-16 15:46:00', '2016-01-16 15:46:00', 27, 1),
(25, '2016-01-16 15:50:00', '2016-01-16 15:50:00', 12, 1),
(26, '2016-01-16 15:53:00', '2016-01-16 15:53:00', 26, 1),
(27, '2016-01-16 15:58:00', '2016-01-16 15:58:00', 22, 1),
(28, '2016-01-16 16:02:00', '2016-01-16 16:02:00', 24, 1),
(29, '2016-01-16 00:00:00', '2016-01-16 00:00:00', 19, 1),
(30, '2016-01-16 00:00:00', '2016-01-16 00:00:00', 7, 1),
(31, '2016-01-16 16:15:00', '2016-01-16 16:15:00', 15, 1),
(32, '2016-01-16 16:18:00', '2016-01-16 16:18:00', 16, 1),
(33, '2016-01-16 16:23:00', '2016-01-16 16:23:00', 17, 1),
(34, '2016-01-16 16:38:00', '2016-01-16 16:38:00', 10, 1),
(35, '2016-01-16 16:49:00', '2016-01-16 16:49:00', 10, 2),
(36, '2016-01-16 16:52:00', '2016-01-16 16:52:00', 25, 1),
(37, '2016-01-16 16:53:00', '2016-01-16 16:53:00', 1, 1),
(38, '2016-01-16 16:53:00', '2016-01-16 16:53:00', 30, 1),
(39, '2016-01-30 21:43:00', '2016-01-30 21:43:00', 31, 1),
(40, '2016-01-30 21:45:00', '2016-01-30 21:45:00', 32, 1),
(41, '2016-01-30 21:45:00', '2016-01-30 21:45:00', 33, 1),
(42, '2016-01-30 21:45:00', '2016-01-30 21:45:00', 34, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `id_noticia` int(11) NOT NULL AUTO_INCREMENT,
  `tituloN` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `copeteN` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `descN` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `imgN` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'defaultN.png',
  `portadaN` tinyint(4) NOT NULL DEFAULT '0',
  `estado` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `iconfont` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_noticia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE IF NOT EXISTS `respuestas` (
  `id_respuesta` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` int(10) unsigned NOT NULL,
  `id_contacto` int(10) unsigned NOT NULL,
  `respuesta` text COLLATE utf8_spanish2_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id_respuesta`),
  KEY `id_usuario` (`id_usuario`,`id_contacto`),
  KEY `id_contacto` (`id_contacto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='Almacena todas las respuestas que una usuario le envia a una cosulta' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$4Y4VsupX3ifqRInwxYqChO675yLcaJBkPZts2OCnrcgqN7PyeoA0i', '', 'admin@admin.com', NULL, NULL, NULL, NULL, 1268889823, 1429993725, 1, 'Adminn', 'teste', 'ADMIN', '0'),
(2, '::1', 'claudio barrios', '$2y$08$4599IN8rSuNblIiaXuHAgOT6aewUtDmCU8AUPuhP1ARnXahoBNz1e', NULL, 'claudio0085@hotmail.com', NULL, NULL, NULL, NULL, 1429390798, 1454186213, 1, 'Claudio', 'Barrios', 'tapir', '4212121'),
(3, '::1', 'gabriel artaza', '$2y$08$wqTcOp958pHgTjDycKm/e.t2Om9i7taHWk1Hgy7/h2hjK9WnEwpTS', NULL, 'buitrehga@gmail.com', NULL, NULL, NULL, NULL, 1429391344, 1429391344, 1, 'Gabriel', 'Artaza', 'tapir', '4212121'),
(4, '201.231.190.57', 'raul frias', '$2y$08$8AvzIoqVilAYjckwyUYzPueLazXAsYiAvK3qkp6GEw.RUSYsGDaRS', NULL, 'raullazarofrias@gmail.com', NULL, NULL, NULL, NULL, 1429992479, 1453577870, 1, 'Raul', 'Frias', 'Tapir', '3874041631');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created` date NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  KEY `uc_users_groups` (`user_id`,`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`, `estado`, `created`, `updated`) VALUES
(1, 1, 1, 0, '2015-04-01', '2015-04-18'),
(2, 2, 2, 0, '2015-04-01', '2015-04-19'),
(3, 3, 2, 1, '2015-04-18', '0000-00-00'),
(4, 1, 1, 1, '2015-04-18', '0000-00-00'),
(5, 2, 2, 0, '2015-04-19', '2015-04-19'),
(6, 2, 1, 0, '2015-04-19', '2016-01-16'),
(7, 4, 2, 0, '2015-04-25', '2015-04-25'),
(8, 4, 1, 1, '2015-04-25', '0000-00-00'),
(9, 2, 2, 0, '2016-01-16', '2016-01-16'),
(10, 2, 1, 1, '2016-01-16', '0000-00-00');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `menues-groups`
--
ALTER TABLE `menues-groups`
  ADD CONSTRAINT `menues-groups_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `menues` (`id_menu`) ON UPDATE CASCADE,
  ADD CONSTRAINT `menues-groups_ibfk_2` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD CONSTRAINT `respuestas_ibfk_1` FOREIGN KEY (`id_contacto`) REFERENCES `contactos` (`id_contacto`),
  ADD CONSTRAINT `respuestas_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `users_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
