<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_config_smtp'))
{
    function get_config_smtp()
   {
        $config_smtp = array(
                    'protocol'      => 'smtp',
                    'smtp_host'     => 'mail.tapir.com.ar',
                    'smtp_port'     => '25',
                    'smtp_timeout'  => '7',
                    'smtp_user'     => 'info@tapir.com.ar',
                    'smtp_pass'     => 'T@p1r201S',
                    'charset'       => 'utf-8',
                    'newline'       => "\r\n",
                    'mailtype'      => 'html',// text or html
                    'validation'    => TRUE// bool whether to validate email or not   
                );                 

        
        return $config_smtp;
    }
}
