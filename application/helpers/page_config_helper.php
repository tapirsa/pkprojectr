<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_config_pagination'))
{
    function get_config_pagination($url="",$cantReg,$nroRegPag,$nroSegmenPag)
   {
        $config_page['base_url'] = base_url().$url;

        $config_page['per_page'] = $nroRegPag;
        $config_page['uri_segment'] = $nroSegmenPag;
        $config_page['total_rows'] = $cantReg;
        $config_page['num_links'] = 1;
        $config_page['page_query_string'] = FALSE;
        $config_page['use_page_numbers'] = TRUE;
        $config_page['suffix'] = "";
        $config_page['query_string_segment'] = 'page';

        $config_page['full_tag_open'] = '<div class="pagination"><ul>';
        $config_page['full_tag_close'] = '</ul></div><!--pagination-->';

        $config_page['first_link'] = '&laquo;';
        $config_page['first_tag_open'] = '<li class="prev page">';
        $config_page['first_tag_close'] = '</li>';

        $config_page['last_link'] = '&raquo;';
        $config_page['last_tag_open'] = '<li class="next page">';
        $config_page['last_tag_close'] = '</li>';

        $config_page['next_link'] = ' &gt;';
        $config_page['next_tag_open'] = '<li class="next page">';
        $config_page['next_tag_close'] = '</li>';

        $config_page['prev_link'] = '&lt; ';
        $config_page['prev_tag_open'] = '<li class="prev page">';
        $config_page['prev_tag_close'] = '</li>';

        $config_page['cur_tag_open'] = '<li class="active"><a href="">';
        $config_page['cur_tag_close'] = '</a></li>';

        $config_page['num_tag_open'] = '<li class="page">';
        $config_page['num_tag_close'] = '</li>';
        $config_page['display_pages'] = TRUE;
        
        return $config_page;
    }
}

