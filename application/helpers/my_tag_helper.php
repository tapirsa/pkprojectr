<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Muestra TODOS errores de validación de un formulario
if ( ! function_exists('my_msj_type')) {

	function my_msj_type($msj="",$type=1) {
            $salida = '';
            if ($msj) {
                switch ($type) {
                    case 1:
                        $salida .= '<div class="alert alert-success alert-dismissable ">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    OK!    </h4> 
                                        '.$msj.'
                                    </div>';
                    break;
                    case 2:
                        $salida .= '<div class="alert alert-info alert-dismissable ">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    Informacion!    </h4> 
                                        '.$msj.'
                                    </div>';
                    break;
                    case 3:
                        $salida .= '<div class="alert alert-danger alert-dismissable ">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    Alerta!    </h4> 
                                        '.$msj.'
                                    </div>';
                    break;
                    case 4:
                        $salida .= '<div class="alert alert-warning alert-dismissable ">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    Alvertencia!    </h4> 
                                        '.$msj.'
                                    </div>';
                    break;
                    default:
                         $salida .= '<div class="alert alert-warning alert-dismissable ">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    Alvertencia!    </h4> 
                                        '.$msj.'
                                    </div>';
                    break;

                }
            }
            return $salida;
	}
}

if ( ! function_exists('my_get_menu')) {

    function my_get_menu($type='h') {
        $out = '';
        $listMenues=array();
        $isLogin = false;
        $parametros = array();
        $CI = &get_instance(); 
        
        if ($CI->ion_auth->logged_in()) {
            $isLogin = true;  
        }
        
        if($type === 'v'){
            // se obtiene el grupo a que pertenece el usuario actualmente
            $grupo = $CI->ion_auth->get_users_groups()->result()[0];
            // se obtiene todos los menues por id de grupo
            $listMenues = $CI->menues_model->getAllByGroupId($grupo->id);
            //filtro operaciones que solo sean de accesos a Administracion y Url de acceso directo
            $listMenuTemporal=array();
            foreach($listMenues as $registro){
                if($registro->url == '' and $registro->accion !== 'principal'){
                    if($registro->showlogin=='1' or $registro->showlogin=='2'){
                        $listMenuTemporal[]=$registro;                      
                    }
                }                
            }
            $listMenues=$listMenuTemporal;
        }
        
        if($type === 'h'){
            $listMenues = $CI->menues_model->getMenuesHorizontales($isLogin);
        }
            
        foreach ($listMenues as $registro) {
            if($registro->url !== ''){         
                $irA = $registro->url;
                $parametros = array('target'=>'_blank');
            } else {
                $irA = $registro->controlador.'/'.$registro->accion;
                $parametros = array('');
            }       
           if($type == 'h'){
                //para saver que menu es el qu se selecciono y poder searlo como activo
                $menuActivo = $CI->uri->segment(1)."/".$CI->uri->segment(2);
                if(strtolower($menuActivo) == strtolower($irA)){
                    $out = $out.'<li class="active">'.anchor($irA,'<span class="separacion-icon-text-menu '.$registro->iconfont.'" aria-hidden="true"></span>'.$registro->name,$parametros).'</li>';
                }else{
                    $out = $out.'<li>'.anchor($irA,'<span class="separacion-icon-text-menu '.$registro->iconfont.'" aria-hidden="true"></span>'.$registro->name,$parametros).'</li>';
                }
            } 
            else {
                $parametros = array('class'=>'list-group-item');
                $out = $out.anchor($irA,$registro->name,$parametros);
            }
        }
        return $out;
    }

}

if ( ! function_exists('my_menu_opUsu')) {

    function my_menu_opUsu() {
        $opciones= array(); 
        $parametros = array('class'=>'btn btn-primary');
        $CI =& get_instance();     
        if ($CI->ion_auth->logged_in()) { // se pregunta se esta logeado
            // se obtiene el grupo a que pertenece el usuario actualmente
            $list = $CI->ion_auth->get_users_groups()->result();
            $grupo = $list[0];
            // se obtiene todos los menues por id de grupo
            $query = $CI->menues_model->getAllByGroupId($grupo->id);
            foreach($query as $registro){
                if($registro->url == '' and $registro->accion !== 'principal'){
                    if($registro->showlogin=='1' or $registro->showlogin=='2'){
                        $irA = $registro->controlador.'/'.$registro->accion;
                        $link=anchor($irA,$registro->name,$parametros);
                        $img=$registro->imgOp;
                        $descripcionOp=$registro->decripcionOp;
                        $opciones[] = array('nombre'=>$registro->name,'link'=>$link,'accion'=>$irA,'img'=>$img ,'decripcionOp'=>$descripcionOp);
                    }
                }                
            }
            return $opciones;
        }       
    }
}

if ( ! function_exists('configArchivoImg')) {

    function configArchivoImg($path){
        //path = './img/imgOpciones/'
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '100';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        $config['remove_spaces']  = true;
        $config['max_filename'] = '20';        
        return $config;
    }

}

if ( ! function_exists('pagination')) {

    function pagination($urlPag="",$cantReg="0",$nroRegPag="10",$nroSegmenPag="3"){
        // se cargan las library y helpers necesarios
        $CI =& get_instance();
        $CI->load->library('pagination');  
        $CI->load->helper('page_config');
        
        $config_page = get_config_pagination($urlPag,$cantReg,$nroRegPag,$nroSegmenPag);
        
        if($CI->uri->segment($nroSegmenPag) && $CI->uri->segment($nroSegmenPag) > 0 ) 
            $config_page['page'] = $CI->uri->segment($nroSegmenPag); 
        else 
            $config_page['page'] = 1;
           
        $CI->pagination->initialize($config_page);
        $config_page['pagination'] = $CI->pagination->create_links();
        
        return $config_page;
    }
}

if ( ! function_exists('redesSociales')) {

    function redesSociales(){?>
        <div class="redes">
            <ul>
                <li>
                    <a href="https://plus.google.com" class="redLink" target="_blank" title="google +">
                        <img src="<?php echo base_url('img/redesSociales/ico-google.png')?>" width="30" height="60" alt="google +">
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com" class="redLink" target="_blank" title="twitter">
                        <img src="<?php echo base_url('img/redesSociales/ico-twitter.png')?>" width="30" height="60" alt="twitter">
                    </a>                        
                </li>
                <li>
                    <a href="https://www.facebook.com" class="redLink" target="_blank" title="facebook">
                        <img src="<?php echo base_url('img/redesSociales/ico-facebook.png')?>" width="30" height="60" alt="facebook">
                    </a>                        
                </li>                        
            </ul>        
        </div>
    <?php
    }
}

?>