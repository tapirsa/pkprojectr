<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Tables extends CI_Migration {

    public function up()
    { 
        
             //$this->db->query("SET FOREIGN_KEY_CHECKS = 1;");
//---------------------------------------------------------------------------------------------------------------------------------
//------------------------- GROUPS -------------------------------------------------------------------------------------------

            // Drop table 'groups' if it exists		
            $this->dbforge->drop_table('groups');

            // Table structure for table 'groups'
            $this->dbforge->add_field(array(
                'id' => array(
                    'type' => 'MEDIUMINT',
                    'constraint' => '8',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ),
                'names' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '30',
                ),
                'description' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                ),
                'estado' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'null' =>FALSE,
                    'default' => '1'
                ),
                'created' => array(
                    'type' => 'DATETIME',
                    'null' => FALSE
                ),
                'updated' => array(
                    'type' => 'DATETIME',
                    'null' => FALSE
                )
            ));
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('groups');

            // Dumping data for table 'groups'
            $data = array(
                array(
                        'id' => '1',
                        'names' => 'admin',
                        'description' => 'Administrator',
                        'estado'=> '1',
                        'created'=>'NOW()',
                        'updated'=>'NOW()'
                ),
                array(
                        'id' => '2',
                        'names' => 'members',
                        'description' => 'General User',
                        'estado'=> '1',
                        'created'=>'NOW()',
                        'updated'=>'NOW()'
                )
            );
            $this->db->insert_batch('groups', $data);
            
            
//---------------------------------------------------------------------------------------------------------------------------------
//------------------------- USERS -------------------------------------------------------------------------------------------

            // Drop table 'users' if it exists
            $this->dbforge->drop_table('users');

            // Table structure for table 'users'
            $this->dbforge->add_field(array(
                    'id' => array(
                            'type' => 'MEDIUMINT',
                            'constraint' => '8',
                            'unsigned' => TRUE,
                            'auto_increment' => TRUE
                    ),
                    'ip_address' => array(
                            'type' => 'VARBINARY',
                            'constraint' => '16'
                    ),
                    'username' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '100',
                    ),
                    'password' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '150',
                    ),
                    'salt' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '40'
                    ),
                    'email' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '100'
                    ),
                    'activation_code' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '40',
                            'null' => TRUE
                    ),
                    'forgotten_password_code' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '40',
                            'null' => TRUE
                    ),
                    'forgotten_password_time' => array(
                            'type' => 'INT',
                            'constraint' => '11',
                            'unsigned' => TRUE,
                            'null' => TRUE
                    ),
                    'remember_code' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '40',
                            'null' => TRUE
                    ),
                    'created_on' => array(
                            'type' => 'INT',
                            'constraint' => '11',
                            'unsigned' => TRUE,
                    ),
                    'last_login' => array(
                            'type' => 'INT',
                            'constraint' => '11',
                            'unsigned' => TRUE,
                            'null' => TRUE
                    ),
                    'active' => array(
                            'type' => 'TINYINT',
                            'constraint' => '1',
                            'unsigned' => TRUE,
                            'null' => TRUE
                    ),
                    'first_name' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '50',
                            'null' => TRUE
                    ),
                    'last_name' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '50',
                            'null' => TRUE
                    ),
                    'company' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '100',
                            'null' => TRUE
                    ),
                    'phone' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '20',
                            'null' => TRUE
                    )

            ));
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('users');

            // Dumping data for table 'users'
            $data = //array( 
                        array(
                            'id' => '1',
                            'ip_address' => 0x7f000001,
                            'username' => 'administrator',
                            'password' => '59beecdf7fc966e2f17fd8f65a4a9aeb09d4a3d4',
                            'salt' => '9462e8eee0',
                            'email' => 'admin@admin.com',
                            'activation_code' => '',
                            'forgotten_password_code' => NULL,
                            'created_on' => '1268889823',
                            'last_login' => '1268889823',
                            'active' => '1',
                            'first_name' => 'Admin',
                            'last_name' => 'istrator',
                            'company' => 'ADMIN',
                            'phone' => '0',
                        );
            $this->db->insert('users', $data);
            
//---------------------------------------------------------------------------------------------------------------------------------
//------------------------- USERS_GROUPS -------------------------------------------------------------------------------------------

            // Drop table 'users_groups' if it exists		
            $this->dbforge->drop_table('users_groups');

            // Table structure for table 'users_groups'
            $this->dbforge->add_field(array(
                'id' => array(
                    'type' => 'MEDIUMINT',
                    'constraint' => '8',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ),
                'user_id' => array(
                    'type' => 'MEDIUMINT',
                    'constraint' => '8',
                    'unsigned' => TRUE
                ),
                'group_id' => array(
                    'type' => 'MEDIUMINT',
                    'constraint' => '8',
                    'unsigned' => TRUE
                ),
                'estado' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'null' =>FALSE,
                    'default' => '1'
                ),
                'created' => array(
                    'type' => 'DATETIME',
                    'null' => FALSE
                ),
                'updated' => array(
                    'type' => 'DATETIME',
                    'null' => FALSE
                )
            ));
            
            $this->dbforge->add_key('id', TRUE);
            
            $this->dbforge->add_foreign_key( array('field' => 'user_id', 'foreign_table' => 'users', 'foreign_field' => 'id'));
            $this->dbforge->add_foreign_key( array('field' => 'group_id', 'foreign_table' => 'groups', 'foreign_field' => 'id'));
                                                
            
            $this->dbforge->create_table('users_groups');

            // Dumping data for table 'users_groups'
            $data = array(
                    array(
                            'id' => '1',
                            'user_id' => '1',
                            'group_id' => '1',
                            'created' => 'NOW()',
                            'updated' => 'NOW()'
                    ),
                    array(
                            'id' => '2',
                            'user_id' => '1',
                            'group_id' => '2',
                            'created' => 'NOW()',
                            'updated' => 'NOW()'
                    )
            );
            $this->db->insert_batch('users_groups', $data);
            
//---------------------------------------------------------------------------------------------------------------------------------
//------------------------- LOGIN_ ATTEMPTS -------------------------------------------------------------------------------------------

            // Drop table 'login_attempts' if it exists
            $this->dbforge->drop_table('login_attempts');

            // Table structure for table 'login_attempts'
            $this->dbforge->add_field(array(
                    'id' => array(
                            'type' => 'INT',
                            'constraint' => '11',
                            'unsigned' => TRUE,
                            'auto_increment' => TRUE
                    ),
                    'ip_address' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '16'
                    ),
                    'login' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '100',
                            'null', FALSE
                    ),
                    'time' => array(
                            'type' => 'INT',
                            'constraint' => '11',
                            'unsigned' => TRUE,
                            'null' => TRUE
                    )
            ));
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('login_attempts');
            
//---------------------------------------------------------------------------------------------------------------------------------
//------------------------- MENUES -------------------------------------------------------------------------------------------

            // Drop table 'menues' if it exists
            $this->dbforge->drop_table('menues');
            
            // Table structure for table 'menues'
            $this->dbforge->add_field(array(
                'id_menu' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '30',
                    'null' => FALSE
                ),
                'created' => array(
                    'type' => 'DATETIME',
                    'null' => FALSE
                ),
                'updated' => array(
                    'type' => 'DATETIME',
                    'null' => FALSE
                ),
                'controlador' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '30',
                    'null' => FALSE
                ),
                'accion' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '30',
                    'null' => FALSE
                ),
                'url' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'null' => TRUE
                ),
                'orden' => array(
                    'type' => 'TINYINT',
                    'constraint' => '5',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ),
                'imgOp' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'null' => FALSE,
                    'default' => 'default.png'
                ),
                'libre' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'null' => FALSE,
                    'default' => '0'
                ),
                'estado' => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'null' => FALSE,
                    'default' => '1'
                ),
                'visible'  => array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'null' => FALSE,
                    'default' => '1'
                ),
                'showlogin'=> array(
                    'type' => 'TINYINT',
                    'constraint' => '3',
                    'null' => FALSE,
                    'default' => '2'
                ),
                'decripcionOp'=> array(
                    'type' => 'TEXT',
                    'null' => FALSE,
                )
            ));
            
            $this->dbforge->add_key('id_menu', TRUE);
            $this->dbforge->create_table('menues');
            
//---------------------------------------------------------------------------------------------------------------------------------
//------------------------- MENUES-GROUPS -------------------------------------------------------------------------------------------
            // Drop table 'menues-groups' if it exists
            $this->dbforge->drop_table('menues-groups');
            
            // Table structure for table 'menues'
            $this->dbforge->add_field(array(
                'id_menu_group' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ),
                'id_menu'=>array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                ),
                'id_group'=>array(
                    'type' => 'MEDIUMINT',
                    'constraint' => '8',
                    'unsigned' => TRUE,
                ),
                'created' => array(
                    'type' => 'DATETIME',
                    'null' => FALSE
                ),
                'updated' => array(
                    'type' => 'DATETIME',
                    'null' => FALSE
                )
            ));
            
            $this->dbforge->add_key('id_menu_group', TRUE);
//            $this->dbforge->add_key('id_menu');
//            $this->dbforge->add_key('id_group');
            
            $this->dbforge->add_foreign_key(array('field' => 'id_menu', 'foreign_table' => 'menues', 'foreign_field' => 'id_menu'));
            $this->dbforge->add_foreign_key(array('field' => 'id_group', 'foreign_table' => 'groups', 'foreign_field' => 'id'));
            
            $this->dbforge->create_table('menues-groups');
            
            
//---------------------------------------------------------------------------------------------------------------------------------
//------------------------- CONCACTOS -------------------------------------------------------------------------------------------

            // Drop table 'contactos' if it exists
            $this->dbforge->drop_table('contactos');
            
            // Table structure for table 'menues'
            $this->dbforge->add_field(array(
                'id_contacto' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ),
                'first_names'=>array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'null' => FALSE,
                ),
                'last_names'=>array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'null' => FALSE,
                ),
                'email'=>array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                    'null' => FALSE,
                ),
                'phone'=>array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'null' => FALSE,
                ),
                'subject'=>array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'null' => FALSE,
                ),
                'mensaje'=>array(
                    'type' => 'TEXT',
                    'null' => FALSE,
                ),
                'estado'=>array(
                    'type' => 'TINYINT',
                    'constraint' => '1',
                    'null' => FALSE,
                    'default' => '1'
                ),
                'was_answered'=>array(
                    'type' => 'TINYINT',
                    'constraint' => '3',
                    'null' => FALSE,
                    'default' => '3',
                ),
                'created' => array(
                    'type' => 'DATETIME',
                    'null' => FALSE
                ),
                'updated' => array(
                    'type' => 'DATETIME',
                    'null' => FALSE
                )
            ));
            
            $this->dbforge->add_key('id_contacto', TRUE);
           
            $this->dbforge->create_table('contactos');
            
            //$this->db->query("SET FOREIGN_KEY_CHECKS = 1;");
            
    }

    public function down()
    {
//        $this->dbforge->drop_table('users');
//        $this->dbforge->drop_table('groups');
//        $this->dbforge->drop_table('users_groups');
//        $this->dbforge->drop_table('login_attempts');
    }
}