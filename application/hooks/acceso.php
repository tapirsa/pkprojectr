<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('autorizar'))
{
    function autorizar()
    {
        $CI =& get_instance();
        $isLogin = false;
        if ($CI->ion_auth->logged_in())  $isLogin = true;

        $libre = $CI->menues_model->menuesLibres($isLogin);
        $controlador = $CI->uri->segment(1);
        $accion = $CI->uri->segment(2);
        $url = $controlador."/".$accion;        
        if(in_array($url, $libre)){
            echo $CI->output->get_output();
        }
        else{
            if ($CI->ion_auth->logged_in()) // seguridad de logeo
            {                
                $idUsu = $CI->ion_auth->get_user_id();                
                $currentGroups = $CI->ion_auth->get_users_groups($idUsu)->result();                
                $menuPorUsu = $CI->menues_grupos_model->menuesPorUsuarios($currentGroups[0]->id);               
                if(in_array($url,$menuPorUsu)){                    
                    echo $CI->output->get_output();
                }
                else{
                    redirect('home/denegar'); 
                }
            }else{
               redirect('home/index');
            }
        }
    }
}