<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Noticias_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
   /* public function getAll($limit=''){
    	$query = $this->db->select('m.id_menu,m.name,m.iconfont,m.controlador,m.accion,m.url,m.orden,case when m.libre = 0 then "NO" else "SI" end  as libre,m.created,m.updated',FALSE)
    			  ->from('menues as m')
    			  ->where('estado','1')
                          ->order_by('orden','asc')
    		          ->get();
    	return $query->result();
    } */
    
    public function getAllPagination($pagConf =array(),$tipoBus="ninguno",$datoBus="nada"){
        $page = $pagConf['page'];
        $cantreg = $pagConf['total_rows'];
        $limit = $pagConf['per_page'];
        $this->db->select(' n.id_noticia,
                            n.tituloN,
                            n.copeteN,
                            n.descN,
                            n.imgN,
                            n.portadaN,
                            n.estado,                         
                            n.created,
                            n.updated',FALSE)
                 ->from('noticias as n')
                 ->where('n.estado','1');
        switch ($tipoBus){
            case "tituloN":            
                 $this->db->like($tipoBus,$datoBus);
            break;            
        }
    	$this->db->order_by('created','desc');//ordeno la consulta
        //CALCULO LA ULTIMA PÁGINA
        $lastpage= ceil( $cantreg/ $limit); //$this->getCantReg()

        //COMPRUEBO QUE EL VALOR DE LA PÁGINA SEA CORRECTO Y SI ES LA ULTIMA PÁGINA
        if($page > $lastpage){
            $page= $lastpage;
        }
       
        $this->db->offset(($page-1) * $limit);
        
        if($cantreg!=0){
            if($limit !== '' && $limit > -1 )
                $this->db->limit($limit); 
        }
        $query = $this->db->get();
    	return $query->result();
    }
    
    public function getCantReg($tipoBus="ninguno",$datoBus="nada"){
        switch ($tipoBus){
            case "ninguno":
                return $result = $this->db->where('estado',1)
                              ->from('noticias')
                              ->count_all_results();
            break;
            case "tituloN":            
                return $result = $this->db->where('estado',1)
                              ->like($tipoBus,$datoBus)
                              ->from('noticias')
                              ->count_all_results();
            break;
        }

        
    }

    public function insert($registro){
        $query = $this->db->set($registro)
                          ->insert('noticias');
        return TRUE;
    }

    public function getFind($id_noticia){
        $query = $this->db->select('id_noticia,tituloN,copeteN,descN,imgN,portadaN,created,updated')
                        ->from('noticias')
                        ->where('id_noticia',$id_noticia)
                        ->get();
        return $query->row();
    }

    public function update($registro){
        $query = $this->db->set($registro)
                        ->where('id_noticia',$registro['id_noticia'])
                        ->update('noticias');
        return TRUE;
    }

    public function deleteLogico($id_noticia) {
        $query = $this->db->set('estado','0')
                          ->where('id_noticia',$id_noticia)
                          ->update('noticias');
        return TRUE;
    }   
}