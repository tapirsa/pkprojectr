
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Respuestas_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function insert($registro){
        $this->db->set($registro)
                 ->insert('respuestas');
        $id = $this->db->insert_id(); 
        return isset($id) ? TRUE : FALSE;
    }

    public function update($registro){
        return  $this->db->set($registro)
                        ->where('id_respuesta',$registro['id_respuesta'])
                        ->update('respuestas');

    }

    public function deleteLogico($id) {
        return $this->db->set('estado','0')
                          ->where('id_respuesta',$id)
                          ->update('respuestas');
    }
    
}
