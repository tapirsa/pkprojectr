<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menues_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function getAll($limit=''){
    	$query = $this->db->select('m.id_menu,m.name,m.iconfont,m.controlador,m.accion,m.url,m.orden,case when m.libre = 0 then "NO" else "SI" end  as libre,m.created,m.updated',FALSE)
    			  ->from('menues as m')
    			  ->where('estado','1')
                          ->order_by('orden','asc')
    		          ->get();
    	return $query->result();
    }
    
    public function getAllPagination($pagConf =array(),$tipoBus="ninguno",$datoBus="nada"){
        $page = $pagConf['page'];
        $cantreg = $pagConf['total_rows'];
        $limit = $pagConf['per_page'];
        $this->db->select(' m.id_menu,
                            m.name,
                            m.controlador,
                            m.accion,
                            m.url,
                            m.orden,
                            case when m.libre = 0 then "NO" else "SI" end  as libre,
                            case when m.libre = 0 then "No corresponde"
                                                  else  case when m.visible = 0 
                                                        then "NO" else "SI" end 
                                                  end   as visible,
                            case when m.libre = 0 then
                                                        case    when m.showlogin = 0 then "NO - (Menu Principal)" 
                                                                when m.showlogin = 1 then "SI - (Menu Principal)" 
                                                                else "SIEMPRE - (Menu Principal)" end
                                                    else  
                                                        case    when m.showlogin = 0 then "NO - (Solo con Login)" 
                                                                when m.showlogin = 1 then "SI - (Solo con Login)" 
                                                                else "SIEMPRE - (Solo con Login)" end
                                                    end as showlogin,
                            m.created,
                            m.updated',FALSE)
                 ->from('menues as m')
                 ->where('estado','1');
        switch ($tipoBus){
            case "name":
            case "controlador":
            case "accion":
            case "url":
                 $this->db->like($tipoBus,$datoBus);
            break;            
        }
    	$this->db->order_by('created','desc');//ordeno la consulta
        //CALCULO LA ULTIMA PÁGINA
        $lastpage= ceil( $cantreg/ $limit); //$this->getCantReg()

        //COMPRUEBO QUE EL VALOR DE LA PÁGINA SEA CORRECTO Y SI ES LA ULTIMA PÁGINA
        if($page > $lastpage){
            $page= $lastpage;
        }
       
        $this->db->offset(($page-1) * $limit);
        
        if($cantreg!=0){
            if($limit !== '' && $limit > -1 )
                $this->db->limit($limit); 
        }
        $query = $this->db->get();
    	return $query->result();
    }
    
    public function getCantReg($tipoBus="ninguno",$datoBus="nada"){
        switch ($tipoBus){
            case "ninguno":
                return $result = $this->db->where('estado',1)
                              ->from('menues')
                              ->count_all_results();
            break;
            case "name":
            case "controlador":
            case "accion":
            case "url":
                return $result = $this->db->where('estado',1)
                              ->like($tipoBus,$datoBus)
                              ->from('menues')
                              ->count_all_results();
            break;
        }

        
    }
    
    public function getAllByGroupId($id){
    	$query = $this->db->select('m.id_menu,m.name,m.controlador,m.accion,m.url,m.imgOp,m.decripcionOp,m.orden,m.showlogin,case when m.libre = 0 then "NO" else "SI" end  as libre,m.created,m.updated',FALSE)
    		          ->from('menues as m')
			          ->where('m.estado','1')
                      ->where('mg.id_group',$id)
                      ->join('`menues-groups` as mg', 'mg.id_menu = m.id_menu')
                      ->order_by('orden','asc')
    			  ->get();
    	return $query->result();

    }

    public function getFind($id_menu){
        $query = $this->db->select('id_menu,name,controlador,accion,url,orden,imgOp,decripcionOp,libre,created,updated,visible,showlogin')
                        ->from('menues')
                        ->where('id_menu',$id_menu)
                        ->get();
        return $query->row();
    }

    public function insert($registro){
        $query = $this->db->set($registro)
                          ->insert('menues');
        return TRUE;
    }

    public function update($registro){
        $query = $this->db->set($registro)
                        ->where('id_menu',$registro['id_menu'])
                        ->update('menues');
        return TRUE;
    }

    public function deleteLogico($id_menu) {
        $query = $this->db->set('estado','0')
                          ->where('id_menu',$id_menu)
                          ->update('menues');
        return TRUE;
    }

    public function accesosDeMenu($id_menu) {
        $query = $this->db->where('id_menu', $id_menu)
                          ->get('menues-groups');
        if($query->num_rows() > 0 ){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function menuesLibres($isLogin=false){
        $libres = array();
        $this->db->select('name,controlador,accion')
                ->where('libre','1')
                ->where('url','')
                ->where('estado','1');

        if(!$isLogin) $this->db->where_in('showlogin',array('0','2'));
        
        $menues = $this->db->get('menues')->result();
        foreach($menues as $menu){
            $libres[$menu->name] = $menu->controlador.'/'.$menu->accion;
        }          
        return $libres;
    }
    
    
    public function  getMenuesHorizontales($isLogin=false){
        $this->db->select('name,iconfont,controlador,accion,url')
                ->where('libre','1')
                ->where('url','')
                ->where('visible','1')
                ->where('estado','1')
                ->order_by('orden','asc');
                
        if($isLogin) $this->db->where_in('showlogin',array('1','2')); else $this->db->where_in('showlogin',array('0','2'));
            
        $query = $this->db->get('menues');

        return $query->result();
    }
}