<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Grupos_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
	// comentario
	
    public function getAll(){
    	$query = $this->db->select('id,name,description,created,updated')
    					->from('groups')
    					->where('estado','1')
                        ->order_by('id','asc')
    					->get();
    	return $query->result();

    }
    
    public function getAllPagination($pagConf =array(),$tipoBus="ninguno",$datoBus="nada"){//1, $limit = 10, $cantreg=0){
        $page = $pagConf['page'];
        $cantreg = $pagConf['total_rows'];
        $limit = $pagConf['per_page'];

        $this->db->select('id,name,description,created,updated')
                 ->from('groups')
                 ->where('estado','1');

        switch ($tipoBus){
            case "name":
            case "description":            
                 $this->db->like($tipoBus,$datoBus);
            break;            
        }
        $this->db->order_by('id','asc');

        //CALCULO LA ULTIMA PÁGINA
        $lastpage= ceil( $cantreg/ $limit); //$this->getCantReg()

        //COMPRUEBO QUE EL VALOR DE LA PÁGINA SEA CORRECTO Y SI ES LA ULTIMA PÁGINA
        if($page > $lastpage){
            $page= $lastpage;
        }
       
        $this->db->offset(($page-1) * $limit);
        
        if($cantreg!=0){
            if($limit !== '' && $limit > -1)
                $this->db->limit($limit); 
        }
        
        $query = $this->db->get();
    	return $query->result();
    }
    
    public function getCantReg($tipoBus="ninguno",$datoBus="nada"){
        switch ($tipoBus){
            case "ninguno":
                return $result = $this->db->where('estado',1)
                              ->from('groups')
                              ->count_all_results();
            break;
            case "name":
            case "description":
                return $result = $this->db->where('estado',1)
                              ->like($tipoBus,$datoBus)
                              ->from('groups')
                              ->count_all_results();                           
            break;
        }
        
    }

    public function getFind($id){
        $query = $this->db->select('id,name,description')
                        ->from('groups')
                        ->where('id',$id)
                        ->get();
        return $query->row();
    }

    public function insert($registro){
        return  $this->db->set($registro)
                         ->insert('groups');
    }

    public function update($registro){
        $query = $this->db->set($registro)
                        ->where('id',$registro['id'])
                        ->update('groups');
        return TRUE;
    }

    public function deleteLogico($id) {
        $query = $this->db->set('estado','0')
                        ->where('id',$id)
                        ->update('groups');
        return TRUE;
    }
    
    public function usuariosDeGrupo($id_grupo) {
        $query = $this->db->where('group_id', $id_grupo)
                        ->where('estado','1')
                        ->get('users_groups');
        if($query->num_rows() > 0 ){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

     public function menuesDeGrupos($id_grupo) {
        $query = $this->db->where('id_group', $id_grupo)
                        ->get('menues-groups');
        if($query->num_rows() > 0 ){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
}