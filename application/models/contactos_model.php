<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contactos_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    public function getAllPagination($pagConf =array(),$tipoBus="ninguno",$datoBus="nada"){
        $page = $pagConf['page'];
        $cantreg = $pagConf['total_rows'];
        $limit = $pagConf['per_page'];

        $this->db->select('id_contacto,first_name,last_name,email,phone,subject,created,was_answered')
    	         ->from('contactos')
                 ->order_by('id_contacto','desc');
        
        switch ($tipoBus){
            case "name":
            case "controlador":
            case "accion":
            case "url":
                 $this->db->like($tipoBus,$datoBus);
            break;            
        }
    	$this->db->order_by('created','desc');//ordeno la consulta
        
        //CALCULO LA ULTIMA PÁGINA
        $lastpage= ceil( $cantreg/ $limit); //$this->getCantReg()

        //COMPRUEBO QUE EL VALOR DE LA PÁGINA SEA CORRECTO Y SI ES LA ULTIMA PÁGINA
        if($page > $lastpage){
            $page= $lastpage;
        }
       
        $this->db->offset(($page-1) * $limit);
        
        if($cantreg!=0){
            if($limit !== '' && $limit > -1 )
                $this->db->limit($limit); 
        }
        $query = $this->db->get();
    	return $query->result();
    }

    public function getAll(){
        $this->db->select(' con.id_contacto,
                            con.first_name,
                            con.last_name,
                            con.mensaje,
                            con.email,
                            con.phone,
                            con.subject,
                            con.created,
                            case con.was_answered  
                                when 1 then \'Respuesta Guardada\'
                                when 2 then \'Contestado via Email\'
                                else \'No contestado\'
                            end was_answered',false)
                 ->from('contactos con')
                 ->where('estado <> 0')
                 ->order_by('created','desc');
        $query = $this->db->get();
        return $query->result();
    }


    
    public function getCantReg($tipoBus="ninguno",$datoBus="nada"){
        switch ($tipoBus){
            case "ninguno":
                return $result = $this->db->where('estado',1)
                              ->from('contactos')
                              ->count_all_results();
            break;
            case "name":
            case "controlador":
            case "accion":
            case "url":
                return $result = $this->db->where('estado',1)
                              ->like($tipoBus,$datoBus)
                              ->from('contactos')
                              ->count_all_results();
            break;
        }
    }
    
    public function getWithAnswer($id){
        $query = $this->db->select('con.id_contacto,
                                    con.first_name,
                                    con.last_name,
                                    con.email,
                                    con.subject,
                                    con.mensaje,
                                    con.was_answered,
                                    IFNULL(res.id_respuesta,\'-1\' ) id_respuesta, 
                                    IFNULL(res.respuesta,\'\' ) respuesta', false)
                          ->from('contactos con')
                          ->join('respuestas res', 'res.id_contacto = con.id_contacto','left')
                          ->where('con.id_contacto',$id)
                          ->get();
        return $query->row();
    }

    public function insert($registro){
        $this->db->set($registro)
                 ->insert('contactos');
        $id = $this->db->insert_id(); 
        return isset($id) ? TRUE : FALSE;
    }



    public function deleteLogico($id) {
        $this->db->set('estado','0')
                 ->where('id_contacto',$id)
                 ->update('contactos');
        return $this->db->affected_rows() > 0 ? TRUE : FALSE;
    }

     public function updateEstado($id, $estado){
        return $this->db->set('was_answered',$estado)
                        ->where('id_contacto',$id)
                        ->update('contactos');
     }


}