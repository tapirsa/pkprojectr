<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menues_grupos_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function getAll(){
    	$query = $this->db->select('mg.id_menu_group, g.name as name_grupo, m.name as name_menu, mg.created as created, mg.updated as updated')
    					->from('menues-groups as mg')
                        ->join('groups as g',' mg.id_group = g.id','left')
                        ->join('menues as m',' mg.id_menu = m.id_menu','left')                        
                        ->order_by('name_grupo','desc')
                        ->get();
        return $query->result();
    }
    
    public function getAllPagination($pagConf =array(),$tipoBus="ninguno",$datoBus="nada"){
        $page = $pagConf['page'];
        $cantreg = $pagConf['total_rows'];
        $limit = $pagConf['per_page'];
        
        $this->db->select('mg.id_menu_group, g.name as name_grupo, m.name as name_menu, mg.created as created, mg.updated as updated ')
                 ->from('menues-groups as mg')
                 ->join('groups as g',' mg.id_group = g.id','left')
                 ->join('menues as m',' mg.id_menu = m.id_menu','left');            
        
        switch ($tipoBus){
            case "m.name":
            case "g.name":
                $this->db->like($tipoBus,$datoBus);
            break;            
        }
        $this->db->order_by('name_grupo','desc');
        //CALCULO LA ULTIMA PÁGINA
        $lastpage= ceil( $cantreg/ $limit); //$this->getCantReg()

        //COMPRUEBO QUE EL VALOR DE LA PÁGINA SEA CORRECTO Y SI ES LA ULTIMA PÁGINA
        if($page > $lastpage){
            $page= $lastpage;
        }
       
        $this->db->offset(($page-1) * $limit);
        
         if($cantreg!=0){
            if($limit !== '' && $limit > -1)
                $this->db->limit($limit); 
        }
        $query = $this->db->get();
    	return $query->result();
     }
    
    public function getCantReg($tipoBus="ninguno",$datoBus="nada"){
        switch ($tipoBus){
            case "ninguno":
                return $result = $this->db->from('menues-groups')
                                         ->count_all_results();             
            break;
            case "m.name":
            case "g.name":
                return $result = $this->db->from('menues-groups as mg')
                              ->join('groups as g',' mg.id_group = g.id','left')
                              ->join('menues as m',' mg.id_menu = m.id_menu','left')
                              ->like($tipoBus,$datoBus)
                              ->count_all_results();
            break;
        }
        
    }

    public function opcionesMenu(){
        $this->load->model('menues_model');
        $menues = $this->menues_model->getAll();
        $opciones = array();
        foreach($menues as $menu){
            $opciones[$menu->id_menu] = $menu->name;
        }
        return $opciones;
    }

    public function opcionesGrupo(){
        $this->load->model('grupos_model');
        $grupos = $this->grupos_model->getAll();
        $opciones = array();
        foreach($grupos as $grupo){
            $opciones[$grupo->id] = $grupo->name;
        }
        return $opciones;
    }

    public function existeCombinacion($registro){
        $query = $this->db->select('id_menu,id_group')
                            ->from('menues-groups')
                            ->where('id_menu',$registro['id_menu'])
                            ->where('id_group',$registro['id_group'])
                            ->get();
        if( $query->num_rows() > 0 ){
            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    public function menuesPorUsuarios($tipoUsu){
        $menuesPorUsu = array();
        $query = $this->db->select('m.name,m.controlador,m.accion')
                ->from('menues-groups as mg')
                ->join('menues as m','mg.id_menu = m.id_menu','left')
                ->where('mg.id_group',$tipoUsu)
                ->get();                
        
        $menues = $query->result();
        foreach($menues as $menu){
            $menuesPorUsu[$menu->name] = $menu->controlador.'/'.$menu->accion;
        }          
        return $menuesPorUsu;
    }

    public function insert($registro){
        $query = $this->db->set($registro)
                        ->insert('menues-groups');
        return TRUE;
    }

    public function getFind($id_menu_gruop){
        $query = $this->db->select('id_menu_group,id_menu,id_group,updated,created')
                        ->from('menues-groups')
                        ->where('id_menu_group',$id_menu_gruop)
                        ->get();
        return $query->row();
    }

    public function update($registro){
        $query = $this->db->set($registro)
                        ->where('id_menu_group',$registro['id_menu_group'])
                        ->update('menues-groups');
        return TRUE;
    }

    public function delete($id_menu_group) {
        $this->db->where('id_menu_group',$id_menu_group);
        if($this->db->delete('menues-groups')){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }    
}