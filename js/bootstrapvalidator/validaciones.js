$(function(){
    
    $('#form_search_table').bootstrapValidator({
        fields:{
            datoBus:{
                validators: {
                    notEmpty:{
                        message: 'Este campo es obligatorio.'
                    }
                }
            }
        }
    });
    
    
    $('#form_create_grupo').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            name:{
                validators: {
                    notEmpty:{
                        message: 'Este campo es obligatorio.'
                    },
                    stringLength: {
                        max: 20,
                        message: 'Por favor, no escribas más de 20 caracteres.'
                    }
                }
            },
            description:{
                validators: {
                    stringLength: {
                        max: 50,
                        message: 'Por favor, no escribas más de 50 caracteres.'
                    }
                }
            }
        }
    });
    
    $('#form_create_menu').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            name:{
                validators: {
                    notEmpty:{
                        message: 'Este campo es obligatorio.'
                    },
                    stringLength: {
                        max: 50,
                        message: 'Por favor, no escribas más de 50 caracteres.'
                    }
                }
            },
            orden:{
                validators: {
                    notEmpty:{
                        message: 'Este campo es obligatorio.'
                    },
                    digits: {
                        message: 'Por favor, escribe sólo dígitos.'
                    }
                }
            }
        }
    });
    
    $('#form_create_grupo').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            group_name: {
               validators: {
                   notEmpty: {
                       message: 'Este campo es obligatorio.'
                   },
                   stringLength: {
                       max: 20,
                       message: 'Por favor, no escribas más de 20 caracteres.'
                   }
               }
           }
       }
    });
    
    
    $('#form_create_user').bootstrapValidator({
       feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    stringLength: {
                        max: 50,
                        message: 'Por favor, no escribas más de 50 caracteres.'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    stringLength: {
                        max: 50,
                        message: 'Por favor, no escribas más de 50 caracteres.'
                    }
                }
            },
            company: {
               validators:{
                   notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    stringLength: {
                        max: 100,
                        message: 'Por favor, no escribas más de 100 caracteres.'
                    }
               }
           },    
           email: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    emailAddress: {
                        message: 'Por favor, escribe una dirección de correo válida.'
                    },
                    stringLength: {
                        max: 100,
                        message: 'Por favor, no escribas más de 100 caracteres.'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    digits: {
                        message: 'Por favor, escribe sólo dígitos.'
                    },
                    stringLength: {
                        max: 20,
                        message: 'Por favor, no escribas más de 20 caracteres.'
                    }
                }
            },
                    
           password: {
               validators:{
                   notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                   identical: {
                        field: 'password_confirm',
                        message: 'Por favor, escribe el mismo valor de nuevo.'
                    }
               }
           },
           password_confirm: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    identical: {
                        field: 'password',
                        message: 'Por favor, escribe el mismo valor de nuevo.'
                    }
                }
            }
        }
    });
    
    $('#form_edit_user').bootstrapValidator({
       feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    stringLength: {
                        max: 50,
                        message: 'Por favor, no escribas más de 50 caracteres.'
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    stringLength: {
                        max: 50,
                        message: 'Por favor, no escribas más de 50 caracteres.'
                    }
                }
            },
            company: {
               validators:{
                   notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    stringLength: {
                        max: 100,
                        message: 'Por favor, no escribas más de 100 caracteres.'
                    }
               }
           },    
           email: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    emailAddress: {
                        message: 'Por favor, escribe una dirección de correo válida.'
                    },
                    stringLength: {
                        max: 100,
                        message: 'Por favor, no escribas más de 100 caracteres.'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    digits: {
                        message: 'Por favor, escribe sólo dígitos.'
                    },
                    stringLength: {
                        max: 20,
                        message: 'Por favor, no escribas más de 20 caracteres.'
                    }
                }
            },
                    
           password: {
               validators:{
                   identical: {
                        field: 'password_confirm',
                        message: 'Por favor, escribe el mismo valor de nuevo.'
                    }
               }
           },
           password_confirm: {
                validators: {
                    identical: {
                        field: 'password',
                        message: 'Por favor, escribe el mismo valor de nuevo.'
                    }
                }
            }
        }
    });
    
    
    $('#form_agregar_contacto').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    }
                }
            },
            last_name: {
                validators: {
                    stringLength: {
                        max: 30,
                        message: 'Por favor, no escribas más de 30 caracteres'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    emailAddress: {
                        message: 'Por favor, escribe una dirección de correo válida.'
                    }
                }
            },
            phone: {
               validators:{
                   digits: {
                        message: 'Por favor, escribe sólo dígitos.'
                    }
               }
           },            
           subject: {
               validators:{
                   notEmpty:{
                       message: 'Este campo es obligatorio.'
                   }
               }
           },
           mensaje: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    }
                }
            },
            captcha:{
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    }
                }
            }
        }
    });
        
})


